# README #

### What is this repository for? ###

* Backend i web en entorn servidor per al projecte print-i-print

### How do I get set up? ###

* Clonar el respositori
* Obrir amb Netbeans o algun altre IDE

#### Enlla�ar amb el projecte frontend

Posem que ambd�s projectes es troben dins la mateixa ubicaci� `/projectes/backend` i `/projectes/frontend`,
es tracta d'enlla�ar el resultat del projecte frontend amb el src del projecte backend.

```
$ cd /projectes/backend/src/main/webapp/WEB-INF
$ ln -s /projectes/frontend/dist private
```

### Contribution guidelines ###

* Crear una branca per US des de master
* Escriure tests
* Realitzar el desenvolupament
* Pull Request cap a la branca principal
* Un company revisa el codi i integra.

### Com accedir a l'API REST protegida amb Token ###

El servidor retornar� un codi d'estat "HTTP/1.1 401 Unauthorized" a totes les peticions a URLs que pengin de /api/*
que no duguin a la cap�alera el camp "X-Auth-Token" que contigui com a valor la cadena de text que representi un "token" v�lid.

Per obtenir un token v�lid cal passar per una etapa d'autenticaci� b�sica que consisteix en enviar, per POST, 
a l'URL .../api/login les credencials d'un usuari (nif/contrasenya).

Si les credencials s�n correctes el servidor retorna un codi "HTTP/1.1 200 OK" i el camp "X-Auth-Token" a la cap�alera,
que contindr� la cadena de text/token que es podr� fer ser� v�lid durant 60 minuts per a autenticar les peticions a l'API.

Procediment d'exemple amb cURL:

1) Fem login i desem el token rebut a la variable X_AUTH_TOKEN:
~~~~
$ X_AUTH_TOKEN=$(curl -i -s -w '\n' -d "nif=12345678A" -d "contrasenya=password1" http://localhost:8080/pintiprint/api/login | grep "X-Auth-Token" | cut -d' ' -f2)
~~~~

2) Fem una consulta a l'API privada posant el token a la cap�alera "X-Auth-Token":
~~~~
$ curl -X GET -i -w '\n' -H "X-Auth-Token:$X_AUTH_TOKEN" http://localhost:8080/pintiprint/api/usuaris/
~~~~

