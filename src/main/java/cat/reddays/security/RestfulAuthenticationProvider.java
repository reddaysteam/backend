package cat.reddays.security;

import cat.reddays.token.TokenAuthenticationService;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import cat.reddays.usuari.Usuari;
import cat.reddays.token.Token;
import cat.reddays.token.TokenGrantedAuthority;
import cat.reddays.token.RestfulToken;
import cat.reddays.token.TokenUtils;

import cat.reddays.token.TokenAuthenticationService;

/**
 * Authentication provider for token based request accepting usernamepasswordAuthenToken as Authentication
 * and return token on the header in response upon Successful authentication using custom AuthenticationSuccessHanlder
 *
 */
public class RestfulAuthenticationProvider implements AuthenticationProvider {

    private static Logger logger = Logger.getLogger(RestfulAuthenticationProvider.class.getName());

    @Autowired
    public TokenAuthenticationService authenticationService;
    
    @Autowired
    TokenUtils tokenUtils;

    @Override
    public Authentication authenticate(Authentication authentication)
                    throws AuthenticationException {
        
        Authentication result = null;
        logger.info("In the authenticate method");
        
        if (!supports(authentication.getClass())){
            return result;
        }

        if (authentication instanceof UsernamePasswordAuthenticationToken){
            result = this.authenticateForNewToken(authentication);
        }

        return result;
    }

    private RestfulToken authenticateForNewToken(final Authentication authentication) {

        RestfulToken restfulToken = null;
                
        if (authenticationService == null) {
            logger.info("Shit happened, nyapa to the rescue...");
            //https://stackoverflow.com/a/19896871
            authenticationService = ApplicationContextHolder.getContext().getBean(TokenAuthenticationService.class);
        }
        
        Usuari usuari = authenticationService.getUsuari(authentication.getName(), 
                authentication.getCredentials().toString());
        
        if (usuari != null) {
            
            logger.info("Getting new token");
        
            //String tokenId = UUID.randomUUID().toString();
            
            if (tokenUtils == null) {
                logger.info("Shit happened, nyapa to the rescue (again)...");
                //https://stackoverflow.com/a/19896871
                tokenUtils = ApplicationContextHolder.getContext().getBean(TokenUtils.class);
            }
            
            UserDetails usuaridetails = authenticationService.getUsuariDetails(usuari.getNif());
            String tokenId = tokenUtils.createToken(usuaridetails);      
        
            
            List<TokenGrantedAuthority> authorities = new ArrayList<TokenGrantedAuthority>();
            
            authorities.add((TokenGrantedAuthority) authenticationService.getAuthority(usuari.getRole().toString()));
            
            Token token = authenticationService.saveToken(authentication.getName(), 
                    authentication.getCredentials().toString(), tokenId, authorities);
            
            restfulToken = new RestfulToken(authorities, tokenId, token.getUsuari());
            
            logger.info("Authentication granted");
        }
        return restfulToken;
    }

    @Override
    public boolean supports(final Class<? extends Object> authentication) {
        
        return RestfulToken.class.isAssignableFrom(authentication) || UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}