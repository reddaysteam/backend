package cat.reddays.security;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;


/**
 * Filter for accepting login requests replacing form login filter
 *
 */
public class RestLoginFilter extends AbstractAuthenticationProcessingFilter {

    public RestLoginFilter() {
        
        super("/api/login");		
    }

    protected RestLoginFilter(String defaultFilterProcessesUrl) {
        
        super(defaultFilterProcessesUrl);		
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                    HttpServletResponse response) throws AuthenticationException,
                    IOException, ServletException {

        logger.info("In the attempt authentication");
        
        Authentication authRequest = null;
        String username = null;
        String password = null;
                
        if (request.getHeader("Content-Type").startsWith("application/json")) {
            
            logger.info("Request for login via JSON");
        
            try {
                
                //HttpServletRequest can be read only once
                 
                StringBuffer sb = new StringBuffer();
                String line = null;

                BufferedReader reader = request.getReader();
                while ((line = reader.readLine()) != null){
                    sb.append(line);
                }
                
                JSONObject json = new JSONObject(sb.toString());
                
                //Get nif and contrasenya from JSON
                username = json.getString("nif");
                password = json.getString("contrasenya");

            } catch (Exception e) {
                
                e.printStackTrace();
            }
            
        } else {
                
            logger.info("Request for login via request paramaeters");

            //Need to save password not in plain text but encrypted.
            //Again for simplicity it is left as plain
            username = request.getParameter("nif");
            password = request.getParameter("contrasenya");        
        }
        
        logger.info("Requested user/pass: " + username +"/"+ password);
        authRequest = new UsernamePasswordAuthenticationToken(username, password);
        
        return this.getAuthenticationManager().authenticate(authRequest);
    }	

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                    FilterChain chain) throws IOException, ServletException {
        
        logger.info("In do filter");
        
        super.doFilter(req, res, chain);		
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                    HttpServletResponse response, FilterChain chain,
                    Authentication authResult) throws IOException, ServletException {

        logger.info("Authentication success. Auth result: " + authResult);

        SecurityContextHolder.getContext().setAuthentication(authResult);
        
        // Fire event
        if (this.eventPublisher != null) {
            eventPublisher.publishEvent(new InteractiveAuthenticationSuccessEvent(authResult, this.getClass()));
        }
        
        super.successfulAuthentication(request, response, chain, authResult);
    }
}