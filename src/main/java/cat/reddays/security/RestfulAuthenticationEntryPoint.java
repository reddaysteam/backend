package cat.reddays.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.logging.Logger;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * Custom entry point return 401 Unauthorized for any request without token
 *
 */
public class RestfulAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private static Logger logger = Logger.getLogger(RestfulAuthenticationEntryPoint.class.getName());

    @Override
    public void commence(HttpServletRequest request,
                    HttpServletResponse response, AuthenticationException authException)
                    throws IOException, ServletException {
        
        logger.info("In the entry point");
        response.sendError( HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized" );

    }
}