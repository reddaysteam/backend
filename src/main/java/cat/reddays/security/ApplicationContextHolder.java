package cat.reddays.security;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * "Manual bean lookup: not recommended"
 * https://stackoverflow.com/a/19896871
 * 
 * This approach is suitable only for interfacing with legacy code in special situations.
 * 
 * Solució per a la fallida creació del token al mètode "authenticateForNewToken" 
 * de la classe RestfulAuthenticationProvider a causa de que no s'hi ha injectat 
 * el bean de l'AuthenticationService malgrat l'@Autowired
 * En cridar aquest mètode, casca en arribar a:
 * "Usuari usuari = authenticationService.getUsuari(authentication.getName(), authentication.getCredentials().toString());"
 * atès que "authenticationService" és null, no s'hi ha injectat el bean... :-(
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;   
    }

    public static ApplicationContext getContext() {
        return context;
    }
}