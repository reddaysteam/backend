package cat.reddays.security;

import cat.reddays.token.TokenAuthenticationService;
import cat.reddays.token.TokenUtils;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.GenericFilterBean;

import cat.reddays.token.Token;
import org.json.JSONObject;

/**
 * Filter for token management replacing session management filter
 *
 */
public class TokenManagementFilter extends GenericFilterBean {

    private static Logger logger = Logger.getLogger(TokenManagementFilter.class.getName());
    
    /**
    * Request attribute that indicates that this filter will not continue with the chain.
    * Handy after login/logout, etc.
    */
    private static final String REQUEST_ATTR_DO_NOT_CONTINUE = "Vade retro";
    
    private final String protectedAPI = "/api/";
    private final String logoutURI = protectedAPI +"logout";

    @Autowired
    private TokenAuthenticationService authenticationService;
    
    @Autowired
    TokenUtils tokenUtils;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                    FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        logger.info("In the tokenManagement filter");
        
        //Controlem si estem accedint a una API protegida
        if (currentLink(request).startsWith(protectedAPI)) {
            
            final String tokenId = request.getHeader("X-Auth-Token");
            logger.info("Token is " + tokenId);
            
            if (tokenId != null) {

                // Dirty hack per corregir la no injecció del bean "authenticationService"
                if (authenticationService == null) {
                    //logger.info("Shit happened, nyapa to the rescue...");
                    //https://stackoverflow.com/a/19896871
                    authenticationService = ApplicationContextHolder.getContext().getBean(TokenAuthenticationService.class);
                }

                Token token = authenticationService.getToken(tokenId);

                if (token != null) {

                    // determine the user based on the (already existing) token
                    String nifUsuari = tokenUtils.getUsuariNifFromToken(tokenId);

                    UserDetails usuariDetails = authenticationService.getUsuariDetails(nifUsuari);

                    // validate the token
                    if (tokenUtils.validate(tokenId, usuariDetails)) {

                        logger.info("Token is valid...");

                        SecurityContextHolder.getContext().setAuthentication(authenticationService.getAuthentication(tokenId));

                        // Controlem si estem fent logout
                        if (currentLink(request).equals(logoutURI)) {

                            logger.info("Logging out, removing Token...");
                            authenticationService.removeToken(tokenId);

                            response.setHeader("Content-Type", "application/json;charset=utf-8");

                            JSONObject outputJsonObj = new JSONObject();
                            outputJsonObj.put("responseCode", "SUCCESS");
                            outputJsonObj.put("message", "You are now logged out... Token's been removed.");

                            response.getWriter().print(outputJsonObj.toString());
                            response.getWriter().flush();
                            
                            response.setStatus(HttpServletResponse.SC_OK);
                            doNotContinueWithRequestProcessing(request);
                        }

                    } else {

                        logger.info("Token NOT valid...");
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                        doNotContinueWithRequestProcessing(request);
                    }

                } else {

                    logger.info("Token NOT found...");
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                    doNotContinueWithRequestProcessing(request);
                }

            } else {

                logger.info("Token NOT received...");

                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                doNotContinueWithRequestProcessing(request);    
            }
        }
        
        if (canRequestProcessingContinue(request)) {
            
            chain.doFilter(request, response);
	}
    }
    
    /**
     * Retorna la URI a la qual estem accedint
     * 
     * @param httpRequest
     * @return 
     */
    private String currentLink(HttpServletRequest httpRequest) {
        
        if (httpRequest.getPathInfo() == null) {
            
            logger.info("Current link: "+ httpRequest.getServletPath());
            
            return httpRequest.getServletPath();
        }
        return httpRequest.getServletPath() + httpRequest.getPathInfo();
    }
    
    /**
    * This is set in cases when we don't want to continue down the filter chain. This occurs
    * for any {@link HttpServletResponse#SC_UNAUTHORIZED} and also for login or logout.
    */
    private void doNotContinueWithRequestProcessing(HttpServletRequest httpRequest) {
        
        httpRequest.setAttribute(REQUEST_ATTR_DO_NOT_CONTINUE, "");
    }

    private boolean canRequestProcessingContinue(HttpServletRequest httpRequest) {
        
        return httpRequest.getAttribute(REQUEST_ATTR_DO_NOT_CONTINUE) == null;
    }
}
