package cat.reddays.security;

import cat.reddays.token.RestfulToken;
import cat.reddays.usuari.Usuari;
import cat.reddays.usuari.client.Client;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.util.StringUtils;

/**
 * Class extending SimpleUrlAuthenticationSuccessHandler working the same
 * way as its parent with the difference of returing return value of 200 with
 * token information instead send direct for restful requests
 *
 */
public class RestAuthenticationSuccessHanlder extends
		SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                    HttpServletResponse response, Authentication authentication)
                    throws IOException, ServletException {

        String targetUrlParam = getTargetUrlParameter();

        if (isAlwaysUseDefaultTargetUrl() || 
            (targetUrlParam != null && 
            StringUtils.hasText(request.getParameter(targetUrlParam)))) {

            clearAuthenticationAttributes(request);

            return;
        }

        RestfulToken tokenInfo = (RestfulToken)authentication;

        response.setHeader("X-Auth-Token", tokenInfo.getTokenId());
        response.setHeader("Content-Type", "application/json;charset=utf-8");

        response.setStatus(HttpServletResponse.SC_OK);
        
        Usuari u = (Usuari)tokenInfo.getPrincipal();
        
        JSONObject outputJsonObj = new JSONObject();
        outputJsonObj.put("nif", u.getNif());
        outputJsonObj.put("nom", u.getNom());
        outputJsonObj.put("cognoms", u.getCognoms());
        outputJsonObj.put("userrole", u.getRole());
        
        if (u.getClass() == Client.class) {
            
            Client c = (Client)u;
            
            outputJsonObj.put("email", c.getEmail());
            outputJsonObj.put("carrer", c.getCarrer());
            outputJsonObj.put("cp", c.getCp());
            outputJsonObj.put("municipi", c.getMunicipi());
            outputJsonObj.put("provincia", c.getProvincia());
            outputJsonObj.put("pais", c.getPais());
            outputJsonObj.put("telefon", c.getTelefon());
            outputJsonObj.put("infoTargetaBanc", c.getInfoTargetaBanc());
        }
        //response.getWriter().print("{\"responseCode\":\"SUCCESS\"}");
        response.getWriter().print(outputJsonObj.toString());
        response.getWriter().flush();
        
        clearAuthenticationAttributes(request);    
    }
}
