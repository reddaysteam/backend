package cat.reddays.login;

/**
 *
 * @author Llorenç Garcia Martinez
 */
public class Login {

    private String nif;
    private String contrasenya;

    public Login() {

    }

    public Login(String nif, String contrasenya) {
        this.nif = nif;
        this.contrasenya = contrasenya;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }
}
