package cat.reddays.usuari;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ColumnTransformer;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author Víctor Llucià Mateu
 * 
 */
@Entity
@Table(name = "usuaris")
@Inheritance(strategy=InheritanceType.JOINED)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Usuari implements Serializable {
    
    @Id
    @NotNull
    @Size(max = 9)
    @Column(name = "nif")
    private String nif;
    
    @NotNull
    @Size(max = 25)
    @Column(name = "nom")
    private String nom;
        
    @NotNull
    @Size(max = 50)
    @Column(name = "cognoms")
    private String cognoms;
    
    @NotNull
    @Size(max = 60)
    @Column(name = "contrasenya")
    @ColumnTransformer(
        read =  "contrasenya",
        write = "crypt( " +
                "    ?, " +
                "    gen_salt('bf',10)" +
                ") "
    )
    //@ColumnTransformer(read = "pgp_sym_decrypt(contrasenya, 'mySecretKey')", write = "pgp_sym_encrypt(?, 'mySecretKey')")
    private String contrasenya;

    public static enum Role {
        ROLE_CLIENT, ROLE_ADMIN, ROLE_OPERARI
    }    

    @Column(name = "userRole")
    @Enumerated(EnumType.STRING)
    private Role userRole;

    public Usuari(String nif, String nom, String cognoms, String contrasenya, Role role) {
        this.nif = nif;
        this.nom = nom;
        this.cognoms = cognoms;
        this.contrasenya = contrasenya;
        this.setRole(role);
    }

    public Usuari() {
    }
    
    public Usuari(String nif){
        this.nif = nif;  
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {

        //this.contrasenya = BCrypt.hashpw(contrasenya, BCrypt.gensalt(10));
        this.contrasenya = contrasenya;
    }
    
    public Boolean login(String contrasenya) {
        
        return BCrypt.checkpw(contrasenya, this.contrasenya);
    }

    public Role getRole() {
        return userRole;
    }

    public void setRole(Role role) {
        this.userRole = role;
    }
        
    @Override
    public String toString() {
        return "Usuari {" + "nif=" + nif + ", nom=" + nom + 
                ", cognoms=" + cognoms + ", contrasenya=" + contrasenya + "}";
    }
}
