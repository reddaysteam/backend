package cat.reddays.usuari.administrador;

import java.util.List;

public class AdministradorService {
    private AdministradorDAO clientDAO;

    public AdministradorService(AdministradorDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    public Administrador get(String nif) {
        return clientDAO.getByNif(nif);
    }
    
    public void add(Administrador usuari) {
        clientDAO.create(usuari);
    }
    
    public List<Administrador> getAll() {
        return clientDAO.getAll();
    }
}
