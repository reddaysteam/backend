package cat.reddays.usuari.administrador;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdministradorController {
    @Autowired
    AdministradorService administradorService;

    @RequestMapping(value = "/api/administradors", method = RequestMethod.GET)
    public List<Administrador> getAllAdministradors() {
        return administradorService.getAll();
    }
    
    @RequestMapping(value = "/api/administradors/{nif}", method = RequestMethod.GET)
    public Administrador getAdministrador(@PathVariable String nif) {
        return administradorService.get(nif);
    }
}
