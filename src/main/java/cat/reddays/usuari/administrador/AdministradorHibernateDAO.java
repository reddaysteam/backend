package cat.reddays.usuari.administrador;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository("administradorHibernateDAO")
public class AdministradorHibernateDAO implements AdministradorDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Administrador getByNif(String nif) {
        return getSession().get(Administrador.class, nif);
    }

    @Override
    public void create(Administrador user) {
        getSession().saveOrUpdate(user);
    }

    @Override
    public Administrador edit(Administrador user) {
        return (Administrador) getSession().merge(user);
    }

    @Override
    public void remove(Administrador user) {
        getSession().delete(user);
    }

    @Override
    public List<Administrador> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Administrador>) criteria.list();
    }
    
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Administrador.class);
    }
}