package cat.reddays.usuari.administrador;

import java.util.List;

public interface AdministradorDAO {
    public Administrador getByNif(String nif);
    public void create(Administrador user);
    public Administrador edit(Administrador user);
    public void remove(Administrador user);
    public List<Administrador> getAll();
}
