package cat.reddays.usuari.administrador;

import cat.reddays.usuari.Usuari;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Víctor Llucià Mateu
 */
@Entity
@Table(name = "administradors")
@PrimaryKeyJoinColumn(name = "nif")
public class Administrador extends Usuari {

    public Administrador() {
    }
    
    public Administrador(String nif, String nom, String cognoms, 
            String contrasenya) {
        
        super(nif, nom, cognoms, contrasenya, Usuari.Role.ROLE_ADMIN);
    }
}