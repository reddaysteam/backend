package cat.reddays.usuari.operari;

import cat.reddays.usuari.Usuari;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Víctor Llucià Mateu
 */
@Entity
@Table(name = "operaris")
@PrimaryKeyJoinColumn(name = "nif")
public class Operari extends Usuari {

    @Column(name = "id_impressora")
    private Integer id_impressora;

    public Operari() {
    }

    public Operari(String nif, String nom, String cognoms, 
            String contrasenya, Integer id_impressora) {
        
        super(nif, nom, cognoms, contrasenya, Usuari.Role.ROLE_OPERARI);
        
        this.id_impressora = id_impressora;
    }
    
    public Integer getId_impressora() {
        return id_impressora;
    }

    public void setId_impressora(Integer id_impressora) {
        this.id_impressora = id_impressora;
    }
}