package cat.reddays.usuari.operari;

import java.util.List;

public class OperariService {
    private OperariDAO clientDAO;

    public OperariService(OperariDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    public Operari get(String nif) {
        return clientDAO.getByNif(nif);
    }
    
    public void add(Operari usuari) {
        clientDAO.create(usuari);
    }
    
    public List<Operari> getAll() {
        return clientDAO.getAll();
    }
}
