package cat.reddays.usuari.operari;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperariController {
    @Autowired
    OperariService administradorService;

    @RequestMapping(value = "/api/operaris", method = RequestMethod.GET)
    public List<Operari> getAllAdministradors() {
        return administradorService.getAll();
    }
}
