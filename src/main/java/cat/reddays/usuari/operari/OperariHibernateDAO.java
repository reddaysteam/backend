package cat.reddays.usuari.operari;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository("operariHibernateDAO")
public class OperariHibernateDAO implements OperariDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Operari getByNif(String nif) {
        return getSession().get(Operari.class, nif);
    }

    @Override
    public void create(Operari user) {
        getSession().saveOrUpdate(user);
    }

    @Override
    public Operari edit(Operari user) {
        return (Operari) getSession().merge(user);
    }

    @Override
    public void remove(Operari user) {
        getSession().delete(user);
    }

    @Override
    public List<Operari> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Operari>) criteria.list();
    }
    
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Operari.class);
    }
}