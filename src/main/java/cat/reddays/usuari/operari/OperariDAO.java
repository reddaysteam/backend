package cat.reddays.usuari.operari;

import java.util.List;

public interface OperariDAO {
    public Operari getByNif(String nif);
    public void create(Operari user);
    public Operari edit(Operari user);
    public void remove(Operari user);
    public List<Operari> getAll();
}
