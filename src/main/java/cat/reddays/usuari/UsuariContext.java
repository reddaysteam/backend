package cat.reddays.usuari;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/** This object wraps {@link Usuari} and makes it {@link UsuariDetails} so that Spring Security can use it. */
public class UsuariContext implements UserDetails {

    private Usuari usuari;

    public UsuariContext(Usuari usuari) {
        
        this.usuari = usuari;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        
        /*
        for (String role : usuari.getRoles()) {
            
            authorities.add(new SimpleGrantedAuthority(role));
        }
        */
        authorities.add(new SimpleGrantedAuthority(usuari.getRole().toString()));
        
        return authorities;
    }

    @Override
    public String getPassword() {
        
        return usuari.getContrasenya();
    }

    @Override
    public String getUsername() {
        
        return usuari.getNif();
    }

    @Override
    public boolean isAccountNonExpired() {
        
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        
        return true;
    }

    @Override
    public boolean isEnabled() {
        
        return true;
    }

    @Override
    public boolean equals(Object o) {
        
        return this == o
                || o != null && o instanceof UsuariContext
                && Objects.equals(usuari, ((UsuariContext) o).usuari);
    }

    @Override
    public int hashCode() {
        
        return Objects.hashCode(usuari);
    }

    @Override
    public String toString() {
        
        return "UsuariContext{" +
                "usuari=" + usuari +
                '}';
    }
}