package cat.reddays.usuari;

import java.util.List;

public interface UsuariDAO {
    
    public Usuari getByNif(String nif);
    public List<Usuari> getAll();
    public void addUsuari(Usuari usuari);
    public void updateUsuari(Usuari usuari);
    public void delete(String nif);
}
