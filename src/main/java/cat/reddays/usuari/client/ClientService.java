package cat.reddays.usuari.client;

import java.util.List;

public class ClientService {
    private ClientDAO clientDAO;

    public ClientService(ClientDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    public Client getClient(String nif) {
        return clientDAO.getByNif(nif);
    }
    
    public void addClient(Client client) {
        clientDAO.create(client);
    }
    
    public List<Client> getAllClients() {
        return clientDAO.getAll();
    }
}
