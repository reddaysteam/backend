package cat.reddays.usuari.client;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository("clientHibernateDAO")
public class ClientHibernateDAO implements ClientDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Client getByNif(String nif) {
        return getSession().get(Client.class, nif);
    }

    @Override
    public void create(Client user) {
        getSession().save(user);
    }

    @Override
    public Client edit(Client user) {
        return (Client) getSession().merge(user);
    }

    @Override
    public void remove(Client user) {
        getSession().delete(user);
    }

    @Override
    public List<Client> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Client>) criteria.list();
    }
    
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Client.class);
    }
}