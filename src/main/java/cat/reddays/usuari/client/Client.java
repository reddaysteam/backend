package cat.reddays.usuari.client;

import cat.reddays.usuari.Usuari;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Víctor Llucià Mateu
 *
 */
@Entity
@Table(name = "clients")
@PrimaryKeyJoinColumn(name = "nif")
public class Client extends Usuari {

    @Column(name = "email")
    @NotNull
    private String email;

    @Column(name = "carrer")
    @NotNull
    private String carrer;

    @Column(name = "cp")
    @NotNull
    private String cp;

    @Column(name = "municipi")
    @NotNull
    private String municipi;

    @Column(name = "provincia")
    @NotNull
    private String provincia;

    @Column(name = "pais")
    @NotNull
    private String pais;

    @Column(name = "telefon")
    @NotNull
    private String telefon;

    @Column(name = "infoTargetaBanc")
    //@NotNull
    private String infoTargetaBanc;

    public Client(String nif, String nom, String cognoms,
            String contrasenya, String carrer, String municipi,
            String cp, String provincia, String pais, String telefon, String email) {

        super(nif, nom, cognoms, contrasenya, Usuari.Role.ROLE_CLIENT);

        this.email = email;
        this.carrer = carrer;
        this.cp = cp;
        this.municipi = municipi;
        this.provincia = provincia;
        this.pais = pais;
        this.telefon = telefon;
        this.infoTargetaBanc = "";
    }

    public Client() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCarrer() {
        return carrer;
    }

    public void setCarrer(String carrer) {
        this.carrer = carrer;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getMunicipi() {
        return municipi;
    }

    public void setMunicipi(String municipi) {
        this.municipi = municipi;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getInfoTargetaBanc() {
        return infoTargetaBanc;
    }

    public void setInfoTargetaBanc(String infoTargetaBanc) {
        this.infoTargetaBanc = infoTargetaBanc;
    }
    
    @Override
    public String toString() {
        return "Client {" + "nif=" + getNif() + ", nom=" + getNom() + 
                ", cognoms=" + getCognoms() + ", contrasenya=" + getContrasenya() + 
                ", email=" + email + ", carrer=" + carrer + ", cp=" + cp +
                ", municipi= "+ municipi +", provincia="+ provincia +
                ", pais="+ pais +", telefon=" + telefon +
                ", infoTargetaBanc="+ infoTargetaBanc +"}";
    }
}
