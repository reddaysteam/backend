package cat.reddays.usuari.client;

import cat.reddays.usuari.Usuari;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {
    @Autowired
    ClientService clientService;

    @RequestMapping(value = "/test_usuaris", method = RequestMethod.GET)
    public ModelAndView getAllUsuaris(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("test_usuaris");
        modelview.getModelMap().addAttribute("usuaris", clientService.getAllClients());
        return modelview;
    }
    
    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView getUsuariByCodi(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ModelAndView modelview = new ModelAndView("signup");
        Client formClient = new Client();
        modelview.getModelMap().addAttribute("formClient", formClient);
        modelview.getModelMap().addAttribute("metaTitle", "Registre");
        modelview.getModelMap().addAttribute("metaDescription", "Formulari d'alta d'usuari a Print-i-print");
        return modelview;
    }
    
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String getUsuariByCodi(@ModelAttribute("formClient") @Validated Client formClient, BindingResult result) {
        formClient.setRole(Usuari.Role.ROLE_CLIENT);
        try {
            clientService.addClient(formClient);
        } catch (Exception e) {
            if (clientService.getClient(formClient.getNif()) != null) {
                result.reject("notNewr", "L'usuari que s'està intentant registrar no és nou");
            } else {
                result.reject("error", e.getMessage());
            }
        }
        
        if (result.hasErrors()) {
            return "signup";
        }
        
        return "redirect:/";
    }
}
