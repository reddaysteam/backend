package cat.reddays.usuari.client;

import java.util.List;

public interface ClientDAO {
    public Client getByNif(String nif);
    public void create(Client user);
    public Client edit(Client user);
    public void remove(Client user);
    public List<Client> getAll();
}
