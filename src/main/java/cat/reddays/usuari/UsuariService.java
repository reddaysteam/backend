package cat.reddays.usuari;

import java.util.List;

public class UsuariService {
    
    private UsuariDAO usuariDAO;

    public UsuariService(UsuariDAO usuariDAO) {
        this.usuariDAO = usuariDAO;
    }

    public Usuari getUsuari(String nif) {
        return usuariDAO.getByNif(nif);
    }
    
    public List<Usuari> getAllUsuaris() {
        return usuariDAO.getAll();
    }
    
    public void delete(String nif) {
        usuariDAO.delete(nif);
    }
}
