package cat.reddays.usuari;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository("usuariHibernateDAO")
public class UsuariHibernateDAO implements UsuariDAO {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Usuari getByNif(String nif) {
        return getSession().get(Usuari.class, nif);
    }
    
    @Override
    public List<Usuari> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Usuari>) criteria.list();
    }    
    
    @Override
    public void addUsuari(Usuari usuari) {
        getSession().saveOrUpdate(usuari);
    }

    @Override
    public void updateUsuari(Usuari usuari) {
        getSession().merge(usuari);
    }
    
    @Override
    public void delete(String nif) {
        Usuari usuari = getByNif(nif);
        getSession().delete(usuari);
    }
    
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Usuari.class);
    }
}
