package cat.reddays.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {
    
    public void init(String user);

    void store(MultipartFile file, String user);

    Stream<Path> loadAll(String user);

    Path load(String filename, String user);

    Resource loadAsResource(String filename, String user);

    void deleteAll(String user);
}
