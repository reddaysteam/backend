package cat.reddays.message;

import java.io.Serializable;

/**
 *
 * @author Llorenç Garcia Martinez
 */
public class Message  implements Serializable {
    private String message;

    public Message() {
        
    }
    
    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
