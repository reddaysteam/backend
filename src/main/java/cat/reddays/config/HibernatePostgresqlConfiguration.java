package cat.reddays.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Aquesta classe:
 * 
 * - Habilita la capacitat de gestionar transaccions amb la base de dades.
 * 
 * - Fa servir els fitxers de propietats jdbc.properties i hibernate.properties
 * 
 * - Importa la classe de configuració HibernateConfiguration
 * 
 * - Crea el Bean dataSource de tipus DataSource amb les propietats del jdbc
 * 
 * NOTA: Depèn de la instal·lació, si no funciona «@PropertySource("jdbc.properties")», 
 *       cal fer servir «@PropertySource("classpath:jdbc.properties")».
 *
 * @author Víctor Llucià Mateu
 */
@Configuration
@EnableTransactionManagement
@PropertySource(value = {"classpath:jdbc.properties", "classpath:hibernate.properties"})
@Import(value = {HibernateConfiguration.class})
public class HibernatePostgresqlConfiguration {
    
    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }
}