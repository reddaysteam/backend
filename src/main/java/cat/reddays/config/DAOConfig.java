package cat.reddays.config;

import cat.reddays.usuari.UsuariDAO;
import cat.reddays.usuari.UsuariHibernateDAO;
import cat.reddays.usuari.administrador.AdministradorDAO;
import cat.reddays.usuari.administrador.AdministradorHibernateDAO;
import cat.reddays.usuari.client.ClientDAO;
import cat.reddays.usuari.client.ClientHibernateDAO;
import cat.reddays.usuari.operari.OperariDAO;
import cat.reddays.usuari.operari.OperariHibernateDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DAOConfig {
    
    @Bean
    public UsuariDAO usuariDAO() {
        return new UsuariHibernateDAO();
    }
    
    @Bean
    public ClientDAO clientDAO() {
        return new ClientHibernateDAO();
    }
    
    @Bean
    public AdministradorDAO administradorDAO() {
        return new AdministradorHibernateDAO();
    }
    
    @Bean
    public OperariDAO operariDAO() {
        return new OperariHibernateDAO();
    }
}
