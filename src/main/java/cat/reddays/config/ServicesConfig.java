package cat.reddays.config;

import cat.reddays.usuari.UsuariDAO;
import cat.reddays.usuari.UsuariService;
import cat.reddays.usuari.administrador.AdministradorDAO;
import cat.reddays.usuari.administrador.AdministradorService;
import cat.reddays.usuari.client.ClientDAO;
import cat.reddays.usuari.client.ClientService;
import cat.reddays.usuari.operari.OperariDAO;
import cat.reddays.usuari.operari.OperariService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServicesConfig {
    
    @Bean
    public UsuariService usuariService(UsuariDAO usuariDAO) {
        return new UsuariService(usuariDAO);
    }
    
    @Bean
    public ClientService clientService(ClientDAO clientDAO) {
        return new ClientService(clientDAO);
    }
    
    @Bean
    public AdministradorService administradorService(AdministradorDAO administradorDAO) {
        return new AdministradorService(administradorDAO);
    }
    
    @Bean
    public OperariService operariService(OperariDAO operariDAO) {
        return new OperariService(operariDAO);
    }         
}
