package cat.reddays.config;

import java.util.Properties;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Aquesta classe:
 * 
 * - Habilita la capacitat de gestionar transaccions amb la base de dades.
 * 
 * - Crea el Bean sessionFactory de tipus LocalSessionFactoryBean 
 *   establint el DataSource del paràmetre.
 * 
 * - Crea el Bean transactionManager de tipus HibernateTransactionManager
 *   assignant-li el SessionFactory del paràmetre
 * 
 * @author Víctor Llucià Mateu
 */
@Configuration
@EnableTransactionManagement
@Import(value = {DAOConfig.class})
public class HibernateConfiguration {
    
    @Autowired
    private Environment environment;

    @Bean
    @Autowired
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) throws NamingException {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan(new String[] { "cat.reddays.usuari", "cat.reddays.token", "cat.reddays.comanda" });
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
        properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
        properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl"));
        properties.put("hibernate.hbm2ddl.import_files", environment.getRequiredProperty("hibernate.hbm2ddl.import_files"));
        properties.put("hibernate.hbm2ddl.import_files_sql_extractor", environment.getRequiredProperty("hibernate.hbm2ddl.import_files_sql_extractor"));
        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }  
}