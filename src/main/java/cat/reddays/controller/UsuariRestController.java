package cat.reddays.controller;

import cat.reddays.message.Message;
import cat.reddays.token.TokenAuthenticationService;
import java.util.List;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cat.reddays.usuari.Usuari;
import cat.reddays.usuari.UsuariDAO;
import cat.reddays.usuari.UsuariService;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/usuaris")
public class UsuariRestController {
    
    private static Logger logger = Logger.getLogger(UsuariRestController.class.getName());
    
    @Autowired
    UsuariService usuariService;
 
    @Autowired
    TokenAuthenticationService authenticationService;
    
    @Autowired
    @Qualifier("usuariHibernateDAO")
    private UsuariDAO usuariDao;

    @RequestMapping(method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Usuari>> listAllMembers()
    {
        ResponseEntity result;
        String nifUsuari = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nifUsuari);
        
        if (usuari.getRole() == Usuari.Role.ROLE_ADMIN) {
            result = new ResponseEntity<>(usuariService.getAllUsuaris(), HttpStatus.OK);
        } else {
            Usuari empty = usuariDao.getByNif("");
            List<Usuari> userList = Arrays.asList(empty);
            result = new ResponseEntity<>(userList, HttpStatus.FORBIDDEN);
        }
        return result;
    }

    @RequestMapping(value="/{nif}", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<Usuari> lookupMemberById(@PathVariable("nif") String nif)
    {
        ResponseEntity result;
        String nifUsuari = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nifUsuari);
        
        if (usuari.getRole() == Usuari.Role.ROLE_ADMIN) {
            
            result = new ResponseEntity<>(usuariDao.getByNif(nif), HttpStatus.OK);
        } else {
            result = new ResponseEntity<>(usuariDao.getByNif(""), HttpStatus.FORBIDDEN);
        }
        return result;
    }
    
    @RequestMapping(value="/{nif}", method=RequestMethod.DELETE, produces="application/json")
    public ResponseEntity<Message> deleteMemberById(@PathVariable("nif") String nif)
    {
        ResponseEntity result;
        String nifUsuari = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nifUsuari);
        
        if (usuari.getRole() == Usuari.Role.ROLE_ADMIN) {
            usuariService.delete(nif);
            result = new ResponseEntity<>(new Message(nif), HttpStatus.OK);
        } else {
            result = new ResponseEntity<>(new Message(""), HttpStatus.FORBIDDEN);
        }
      
        return result;
    }
}