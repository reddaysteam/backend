package cat.reddays.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("index");

        modelview.getModelMap().addAttribute("imagePrinters", "Printers_150x80.png");
        modelview.getModelMap().addAttribute("imageDelivery", "Delivery_150x80.png");
        modelview.getModelMap().addAttribute("imageCarousel_1", "image1.png");
        modelview.getModelMap().addAttribute("imageCarousel_2", "image2.png");
        modelview.getModelMap().addAttribute("metaTitle", "Home");
        modelview.getModelMap().addAttribute("metaDescription", "Pint-i-print és un servei d'impressió 3D online");
        
        return modelview;
    }
    
    @RequestMapping(value = "/contacte", method = RequestMethod.GET)
    public ModelAndView handleContacteRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("contacte");
        
        modelview.getModelMap().addAttribute("metaTitle", "Contacte");
        modelview.getModelMap().addAttribute("metaDescription", "Com contactar amb Pint-i-print");

        return modelview;
    }

    @RequestMapping(value = "/faq", method = RequestMethod.GET)
    public ModelAndView faq(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("faq");
        
        modelview.getModelMap().addAttribute("metaTitle", "FAQs");
        modelview.getModelMap().addAttribute("metaDescription", "Preguntes freqüents");

        return modelview;
    }
    
    @RequestMapping(value = "/lopd", method = RequestMethod.GET)
    public ModelAndView lopd(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("lopd");
        
        modelview.getModelMap().addAttribute("metaTitle", "Avís legal");
        modelview.getModelMap().addAttribute("metaDescription", "Avís legal");

        return modelview;
    }
    
    @RequestMapping(value = "/cookies", method = RequestMethod.GET)
    public ModelAndView cookies(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("cookies");
        
        modelview.getModelMap().addAttribute("metaTitle", "Ús de les cookies");
        modelview.getModelMap().addAttribute("metaDescription", "Informació sobre l'ús de les cookies");

        return modelview;
    }

    @RequestMapping(value = "/accessibilitat", method = RequestMethod.GET)
    public ModelAndView accessibilitat(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ModelAndView modelview = new ModelAndView("accessibilitat");
        
        modelview.getModelMap().addAttribute("metaTitle", "Accessibilitat");
        modelview.getModelMap().addAttribute("metaDescription", "Informació sobre l'accessibilitat a Pint-i-Print");

        return modelview;
    }
}
