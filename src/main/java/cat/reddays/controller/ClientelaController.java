package cat.reddays.controller;

 import java.util.List;

import javax.validation.Valid;

import cat.reddays.usuari.client.Client;
import cat.reddays.usuari.client.ClientDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/clientela")
public class ClientelaController {
    
    @Autowired
    @Qualifier("clientHibernateDAO")
    private ClientDAO clientDao;

    @RequestMapping(method=RequestMethod.GET)
    public String displaySortedMembers(Model model) {
        
        model.addAttribute("nouClient", new Client());
        model.addAttribute("clients", clientDao.getAll());
        return "index";
    }

    @RequestMapping(method=RequestMethod.POST)
    public String afegirNouClient(@Valid @ModelAttribute("nouClient") Client nouClient, BindingResult result, Model model) {
        
        if (!result.hasErrors()) {
            clientDao.create(nouClient);
            return "redirect:/";
        }
        else {
            model.addAttribute("clients", clientDao.getAll());
            return "index";
        }
    }
    
//    @RequestMapping(value="login", method=RequestMethod.POST, produces="application/json")
//    public @ResponseBody List<Member> login(@RequestParam String username, @RequestParam String password)
//    {
//        return memberDao.findAllOrderedByName();
//    }
}
