package cat.reddays.token;

public interface TokenDAO {

    public void addToken(Token token);

    public Token getToken(String id);
    
    public void removeToken(Token token);
}
