package cat.reddays.token;

import cat.reddays.usuari.Usuari;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tokens")
public class Token implements Serializable {

    @Id
    private String token;

    @OneToOne(targetEntity=Usuari.class)
    @JoinColumn(name="nif")
    private Usuari usuari;

    @ManyToMany(targetEntity=TokenGrantedAuthority.class)
    @JoinColumn(name="role_id")
    private List<TokenGrantedAuthority> authorities;

    public Token() {

    }

    public Token(String token){
        
        this.token = token;
    }

    public String getToken() {
        
        return token;
    }

    public void setToken(String token) {
        
        this.token = token;
    }

    public Usuari getUsuari() {
        
        return usuari;
    }

    public void setUsuari(Usuari usuari) {
        
        this.usuari = usuari;
    }

    public void setAuthorities(List<TokenGrantedAuthority> authorities) {
        
        this.authorities = authorities;
    }

    public List<TokenGrantedAuthority> getAuthorities() {
        
        return authorities;
    }
    
    /**
     * 
     */
    private static final long serialVersionUID = -5254049518157312910L;
}
