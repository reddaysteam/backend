package cat.reddays.token;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import cat.reddays.token.RestfulToken;
import cat.reddays.usuari.Usuari;
import cat.reddays.token.Token;
import cat.reddays.token.TokenGrantedAuthority;

/**
 * Service for providing Token and Authentication management methods
 *
 */
public interface TokenAuthenticationService {

    public Token getToken(String id);
    public Token saveToken(String nif, String contrasenya, String tokenId, List<TokenGrantedAuthority> authorities);
    public void removeToken(String tokenId);
    public Authentication saveAuthentication(Authentication authentication);
    public GrantedAuthority getAuthority(String role);
    public RestfulToken getAuthentication(String tokenId);
    public Usuari getUsuari(String nif, String contrasenya);
    public UserDetails getUsuariDetails(String nif);
    public UserDetails currentUser();
}
