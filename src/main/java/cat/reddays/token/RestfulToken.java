package cat.reddays.token;

import java.io.Serializable;
import java.util.List;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import cat.reddays.usuari.Usuari;
import cat.reddays.token.TokenGrantedAuthority;

/**
 * Extended Authentication Object for transfering token information between filters
 *
 */
public class RestfulToken extends AbstractAuthenticationToken implements Serializable {

    private Object credentials;
    
    private Object principal;

    private String tokenId;

    public RestfulToken(List<TokenGrantedAuthority> authorities, final String tokenId, Usuari usuari) {
        
        super(authorities);
        
        this.tokenId = tokenId;
        this.principal = usuari;
        setAuthenticated(true);
    }

    public RestfulToken(String tokenId) {

        super(null);

        this.tokenId = tokenId;
        setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {

        return this.credentials;
    }

    @Override
    public Object getPrincipal() {

        return this.principal;
    }

    public String getTokenId() {
        
        return this.tokenId;
    }
    
    /**
     * 
     */
    private static final long serialVersionUID = -7387968508541715234L;
}
