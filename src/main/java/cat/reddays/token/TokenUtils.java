package cat.reddays.token;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.springframework.stereotype.Component;

@Component
public class TokenUtils {
    
    public static final String MAGIC_KEY = "redsecretdays";

    public static String createToken(UserDetails usuariDetails) {
        
        /* Expires in one hour */
        long expires = System.currentTimeMillis() + 1000L * 60 * 60;

        StringBuilder tokenBuilder = new StringBuilder();
        tokenBuilder.append(usuariDetails.getUsername());
        tokenBuilder.append(":");
        tokenBuilder.append(expires);
        tokenBuilder.append(":");
        tokenBuilder.append(TokenUtils.computeSignature(usuariDetails, expires));

        return tokenBuilder.toString();
    }

    public static String computeSignature(UserDetails usuariDetails, long expires) {
        
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(usuariDetails.getUsername());
        signatureBuilder.append(":");
        signatureBuilder.append(expires);
        signatureBuilder.append(":");
        signatureBuilder.append(usuariDetails.getPassword());
        signatureBuilder.append(":");
        signatureBuilder.append(TokenUtils.MAGIC_KEY);

        MessageDigest digest;
        
        try {
            
            digest = MessageDigest.getInstance("SHA-256");
            
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("No SHA-256 algorithm available!");
        }

        return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
    }

    public static String getUsuariNifFromToken(String authToken) {
        
        if (null == authToken) {
            
            return null;
        }

        String[] parts = authToken.split(":");
        return parts[0];
    }

    public static boolean validate(String authToken, UserDetails userDetails) {
                
        String[] parts = authToken.split(":");
        
        long expires = Long.parseLong(parts[1]);
        
        String signature = parts[2];

        if (expires < System.currentTimeMillis()) {
            
            return false;
        }

        return signature.equals(TokenUtils.computeSignature(userDetails, expires));
    }
}