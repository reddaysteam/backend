package cat.reddays.token;

import java.util.List;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cat.reddays.usuari.Usuari;
import cat.reddays.usuari.UsuariDAO;
import cat.reddays.usuari.UsuariContext;
import cat.reddays.token.Token;
import cat.reddays.token.TokenDAO;
import cat.reddays.token.TokenGrantedAuthority;
import cat.reddays.token.TokenGrantedAuthorityDAO;
import cat.reddays.token.RestfulToken;
import cat.reddays.token.TokenUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;

@Service
@Transactional
public class TokenAuthenticationServiceImpl implements TokenAuthenticationService {

    private static Logger logger = Logger.getLogger(TokenAuthenticationServiceImpl.class.getName());

    @Autowired
    @Qualifier("usuariHibernateDAO")
    private UsuariDAO usuariDao;

    @Autowired
    private TokenDAO tokenDao;
    
    @Autowired
    TokenUtils tokenUtils;

    @Autowired
    private TokenGrantedAuthorityDAO tokenGrantedAuthorityDao;
    
    @Override
    @Transactional
    public Token getToken(String id) {

        return tokenDao.getToken(id);
    }

    @Override
    @Transactional
    public Token saveToken(String nif, String contrasenya, String tokenId, List<TokenGrantedAuthority> authorities) {

        logger.info("Saving token");

        Token token = new Token();

        Usuari usuari = usuariDao.getByNif(nif);

        token.setUsuari(usuari);
        token.setAuthorities(authorities);
        token.setToken(tokenId);

        tokenDao.addToken(token);

        return token;
    }
    
    @Override
    @Transactional
    public void removeToken(String tokenId) {
        
        tokenDao.removeToken(this.getToken(tokenId));
    }

    @Override
    @Transactional
    public Authentication saveAuthentication(Authentication authentication) {

        String tokenId = ((RestfulToken) authentication).getTokenId();
        logger.info("Token info: " + tokenId);

        Token existing = tokenDao.getToken(tokenId);

        if (existing == null){
            logger.warning("Token expired or does not exist, login again");
        }

        if (existing.getUsuari() == null){
            logger.severe("Token does not belong to any user");
        }

        return new RestfulToken(existing.getAuthorities(), existing.getToken(), existing.getUsuari());
    }

    @Override
    public GrantedAuthority getAuthority(String role) {

        return tokenGrantedAuthorityDao.getAuthority(role);
    }

    @Override
    public RestfulToken getAuthentication(String tokenId) {

        RestfulToken authentication = null;

        Token existing = tokenDao.getToken(tokenId);

        if (existing != null){
            authentication = new RestfulToken(existing.getAuthorities(), existing.getToken(), existing.getUsuari());
        }

        return authentication;
    }

    @Override
    public Usuari getUsuari(String nif, String contrasenya) {

        Usuari u = usuariDao.getByNif(nif);
                
        //Comparem la contrasenya rebuda amb l'emmagatzemada per l'usuari
        if (BCrypt.checkpw(contrasenya, u.getContrasenya())) {

            logger.info("Contrasenya correcta. Subclasse d'Usuari ---> "+ u.getClass().getName());
            
        }  else {
            
            u = null;
            logger.warning("Contrasenya incorrecta ---> Usuari: "+ nif +", Contrasenya: "+ contrasenya);
        }
        
        return u;
    }      
    
    @Override
    public UserDetails getUsuariDetails(String nif) throws UsernameNotFoundException {
        
        Usuari usuari = usuariDao.getByNif(nif);
        
        if (usuari == null) {
            
            throw new UsernameNotFoundException("User " + nif + " not found");
        }
        return new UsuariContext(usuari);
    }
    
    @Override
    public UserDetails currentUser() {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        if (authentication == null) {
                return null;
        }
                
        Usuari usuari = (Usuari)authentication.getPrincipal();
        
        return new UsuariContext(usuari);
    }
}