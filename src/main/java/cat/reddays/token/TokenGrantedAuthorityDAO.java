package cat.reddays.token;

public interface TokenGrantedAuthorityDAO {

    public TokenGrantedAuthority getAuthority(String role);
}