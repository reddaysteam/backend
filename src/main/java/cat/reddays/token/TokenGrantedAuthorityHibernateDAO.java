package cat.reddays.token;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public class TokenGrantedAuthorityHibernateDAO implements TokenGrantedAuthorityDAO {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public TokenGrantedAuthority getAuthority(String role) {
        
        return getSession().get(TokenGrantedAuthority.class, role);
    }
    
    protected Session getSession() {
        
        return sessionFactory.getCurrentSession();
    }
}