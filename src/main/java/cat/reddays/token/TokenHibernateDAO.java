package cat.reddays.token;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TokenHibernateDAO implements TokenDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addToken(Token token) {
        
        getSession().saveOrUpdate(token);
    }

    @Override
    public Token getToken(String id) {
    
        return getSession().get(Token.class, id);
    }
    
    @Override
    public void removeToken(Token token) {
        
        getSession().remove(token);
    }
    
    protected Session getSession() {
        
        return sessionFactory.getCurrentSession();
    }
}
