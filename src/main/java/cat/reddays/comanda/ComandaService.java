package cat.reddays.comanda;

import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ComandaService {
    
    private ComandaDAO comandaDAO;

    public ComandaService(ComandaDAO comandaDAO) {
        this.comandaDAO = comandaDAO;
    }

    public Comanda getComandaByNif(String nif) {
        return comandaDAO.getByNif(nif);
    }
    public Comanda getComandaById(Long id) {
        return comandaDAO.getById(id);
    }
    
    public void addComanda(Comanda comanda) {
        comandaDAO.create(comanda);
    }
    
    public void updateComanda(Comanda comanda) {
        comandaDAO.update(comanda);
    }
    
    public List<Comanda> getAllComandes() {
        return comandaDAO.getAll();
    }
    
    public List<Comanda> getAllByClient(String nif) {
        return comandaDAO.getAllByClient(nif);
    }
}
