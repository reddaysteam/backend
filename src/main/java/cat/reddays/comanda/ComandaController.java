package cat.reddays.comanda;

import cat.reddays.storage.StorageFileNotFoundException;
import cat.reddays.storage.StorageService;
import cat.reddays.token.TokenAuthenticationService;
import cat.reddays.usuari.Usuari;
import cat.reddays.usuari.UsuariService;
import cat.reddays.usuari.client.Client;
import cat.reddays.usuari.client.ClientService;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/api/comanda")
public class ComandaController {
    
    private static Logger logger = Logger.getLogger(ComandaController.class.getName());
    
    @Autowired
    ComandaService comandaService;
    
    @Autowired
    ClientService clientService;
    
    @Autowired
    UsuariService usuariService;
    
    @Autowired
    StorageService storageService;
    
    @Autowired
    TokenAuthenticationService authenticationService;
    
    
    @RequestMapping(method=RequestMethod.GET, produces="application/json")
    public @ResponseBody List<Comanda> listAllComandes() {
        List<Comanda> result;
        String nif = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nif);
        
        logger.info("Retrieving comandes");
        if(usuari.getRole() == Usuari.Role.ROLE_CLIENT) {
            result = comandaService.getAllByClient(nif);
        } else {
            result = comandaService.getAllComandes();
        }

        return result;
    }
    
    
    @RequestMapping(value = "/uploadform", method = RequestMethod.GET)
    public String listUploadedFiles(Model model) throws IOException {
        
        String nif = authenticationService.currentUser().getUsername();
        
        storageService.init(nif);

        model.addAttribute("files", storageService.loadAll(nif).map(
                path -> MvcUriComponentsBuilder.fromMethodName(ComandaController.class,
                        "serveFile", path.getFileName().toString()).build().toString())
                .collect(Collectors.toList()));

        return "uploadForm";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Comanda> create(@RequestBody Comanda comanda) {
        
        String nif = authenticationService.currentUser().getUsername();
        Client client = clientService.getClient(nif);

        Date dataCreacio = new Date();
        comanda.setClient(client);
        comanda.setDataCreacio(dataCreacio);
        comanda.setDataEnviament(null);
        comanda.setEstat(Estat.PENDENT);
        
        comandaService.addComanda(comanda);
        
        return new ResponseEntity<>(comanda, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "{codi}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Comanda> read(@PathVariable Long codi) {
        ResponseEntity result;
        String nif = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nif);
        Comanda comanda = this.comandaService.getComandaById(codi);
        
        if (comanda == null) {
            result = new ResponseEntity<>(new Comanda(), HttpStatus.NOT_FOUND);
        } else if (usuari.getRole() != Usuari.Role.ROLE_CLIENT || comanda.getClient().getNif().equals(usuari.getNif())) {
            result = new ResponseEntity<>(comanda, HttpStatus.OK);
        } else {
            result = new ResponseEntity<>(new Comanda(), HttpStatus.FORBIDDEN);
        }
        
        return result;
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public @ResponseBody
    ResponseEntity<Comanda> update(@RequestBody Comanda comanda) {
        ResponseEntity result;
        String nif = authenticationService.currentUser().getUsername();
        Usuari usuari = usuariService.getUsuari(nif);
        
        if (usuari.getRole() != Usuari.Role.ROLE_CLIENT) {
            this.comandaService.updateComanda(comanda);
            result = new ResponseEntity<>(comanda, HttpStatus.OK);
        } else {
            result = new ResponseEntity<>(new Comanda(), HttpStatus.FORBIDDEN);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/fileupload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE + ";charset=utf-8")
    public @ResponseBody String handleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) {

        String nif = authenticationService.currentUser().getUsername();
        
        storageService.init(nif);
        
        storageService.store(file, nif);
/*
        String uri = MvcUriComponentsBuilder.fromMethodName(ComandaController.class,
                        "serveFile", file.getOriginalFilename().toString()).build().toString();
*/
        String uri = StringUtils.cleanPath(file.getOriginalFilename());
        logger.info(uri);
        
        JSONObject outputJsonObj = new JSONObject();
        outputJsonObj.put("uri", uri);
        
        return outputJsonObj.toString();
    }
    
    @RequestMapping(value="{comandaId}/file", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable Long comandaId) {

        // String nif = authenticationService.currentUser().getUsername();
        
        Comanda comanda = comandaService.getComandaById(comandaId);
        Client client = comanda.getClient();
        
        storageService.init(client.getNif());
        
        Resource file = storageService.loadAsResource(comanda.getPathFitxer3d(), client.getNif());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"");
        headers.add("x-filename", file.getFilename());

        try {
            URI uri = file.getURI();
            Path path = Paths.get(uri.getPath());
            headers.add("content-type", Files.probeContentType(path));
        } catch (IOException ex) {
            Logger.getLogger(ComandaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ResponseEntity.ok().headers(headers).body(file);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}
