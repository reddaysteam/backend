package cat.reddays.comanda;

import cat.reddays.usuari.client.Client;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Transactional
@Repository("comandaHibernateDAO")
public class ComandaHibernateDAO implements ComandaDAO {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public Comanda getByNif(String nif) {
        return getSession().get(Comanda.class, nif);
    }
    
    @Override
    public Comanda getById(Long id) {
        return getSession().get(Comanda.class, id);
    }

    @Override
    public void create(Comanda comanda) {
        getSession().save(comanda);
    }
    
    @Override
    public void update(Comanda comanda) {
        getSession().update(comanda);
    }

    @Override
    public Comanda edit(Comanda comanda) {
        return (Comanda) getSession().merge(comanda);
    }

    @Override
    public void remove(Comanda comanda) {
        getSession().delete(comanda);
    }

    @Override
    public List<Comanda> getAll() {
        Criteria criteria = createEntityCriteria();
        return (List<Comanda>) criteria.list();
    }
    
    @Override
    public List<Comanda> getAllByClient(String nif) {
        
        /*
        In HQL, you should use the java class name and property name 
        of the mapped @Entity instead of the actual table name and column name
        */
        StringBuilder query = new StringBuilder("FROM Comanda ");
        
        query.append("WHERE id_client ='"+ nif +"' ");
        
        query.append("ORDER BY dataCreacio");
        
        Query result = getSession().createQuery(query.toString());

        return result.list();
    }
    
    
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private Criteria createEntityCriteria() {
        return getSession().createCriteria(Comanda.class);
    }
    
}
