package cat.reddays.comanda;

import cat.reddays.usuari.client.Client;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "comandes")
public class Comanda {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="id_client")
    private Client client;
    
    private int quantitat;    
    private double costUnitari;    
    private double subTotal;
    private String pathFitxer3d;
    
    @DateTimeFormat(pattern="yyyy.MM.dd-hh:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataCreacio;
    @DateTimeFormat(pattern="yyyy.MM.dd-hh:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataEnviament;    
    @Enumerated(EnumType.STRING)
    private Estat estat;
    
    public Comanda(Client client, int quantitat, String pathFitxer3d,
            Date dataCreacio, Date dataEnviament, Estat estat) {
        
        this.client = client;
        this.quantitat = quantitat;
        this.pathFitxer3d = pathFitxer3d;
        this.dataCreacio = dataCreacio;
        this.dataEnviament = dataEnviament;
        this.estat = estat;
    }
    
    public Comanda(Client client, int quantitat, double costUnitari,
			double subTotal, String pathFitxer3d, Date dataCreacio,
                        Date dataEnviament, Estat estat) {
        
        this.client = client;
        this.quantitat = quantitat;
        this.costUnitari = costUnitari;
        this.subTotal = subTotal;
        this.pathFitxer3d = pathFitxer3d;
        this.dataCreacio = dataCreacio;
        this.dataEnviament = dataEnviament;
        this.estat = estat;
    }
    
    public Comanda(int quantitat, String pathFitxer3d) {
        this.quantitat = quantitat;
        this.pathFitxer3d = pathFitxer3d;
    }
    
    public Comanda() {

    }   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(int quantitat) {
        this.quantitat = quantitat;
    }

    public double getCostUnitari() {
        return costUnitari;
    }

    public void setCostUnitari(float costUnitari) {
        this.costUnitari = costUnitari;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public String getPathFitxer3d() {
        return pathFitxer3d;
    }

    public void setPathFitxer3d(String pathFitxer3d) {
        this.pathFitxer3d = pathFitxer3d;
    }

    public Date getDataCreacio() {
        return dataCreacio;
    }

    public void setDataCreacio(Date dataCreacio) {
        this.dataCreacio = dataCreacio;
    }

    public Date getDataEnviament() {
        return dataEnviament;
    }

    public void setDataEnviament(Date dataEnviament) {
        this.dataEnviament = dataEnviament;
    }

    public Estat getEstat() {
        return estat;
    }

    public void setEstat(Estat estat) {
        this.estat = estat;
    }
}
