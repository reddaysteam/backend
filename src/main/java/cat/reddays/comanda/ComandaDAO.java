package cat.reddays.comanda;

import cat.reddays.usuari.client.Client;
import java.util.List;

public interface ComandaDAO {
    
    public Comanda getByNif(String nif);
    public Comanda getById(Long id);
    public void create(Comanda comanda);
    public void update(Comanda comanda);
    public Comanda edit(Comanda comanda);
    public void remove(Comanda comanda);
    public List<Comanda> getAll();
    public List<Comanda> getAllByClient(String nif);    
}
