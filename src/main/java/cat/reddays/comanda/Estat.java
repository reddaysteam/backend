package cat.reddays.comanda;

public enum Estat {
    
    PENDENT("Pendent"),
    ENPROCES("En procés"),
    ENVIADA("Enviada"),
    ENTREGADA("Entregada"),
    CANCELLADA("Cancel·lada"),
    DENEGADA("Denegada");

    private String codi;

    Estat(String codi) {
        this.codi = codi;
    }

    public String getCode() {
        return codi;
    }

    public static Estat fromCode(String codi) {
        for (Estat estat : Estat.values()){
            if (estat.getCode().equals(codi)){
                return estat;
            }
        }
        throw new UnsupportedOperationException(
                "El codi " + codi + " no existeix!");
    }
}
