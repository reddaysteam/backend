/*
DROP DATABASE IF EXISTS pintiprint;

CREATE DATABASE pintiprint OWNER pinti;

\c pintiprint;

CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TYPE usuari AS (
    nif VARCHAR(9),
    nom VARCHAR(25),
    cognoms VARCHAR(50),
    contrasenya VARCHAR(60)
);

DROP TYPE IF EXISTS estat_comanda;
CREATE TYPE estat_comanda AS ENUM ('pendent', 'en procés', 'enviada', 'entregada', 'cancel·lada');

CREATE TABLE usuaris OF usuari (
    CONSTRAINT "PK_usuari" PRIMARY KEY (nif)
);

CREATE TABLE clients (
    email VARCHAR(50),
    carrer VARCHAR(75),
    cp VARCHAR(75),
    municipi VARCHAR(75),
    provincia VARCHAR(75),
    pais VARCHAR(75),
    telefon VARCHAR(15),
    infoTargetaBanc VARCHAR(50),
    CONSTRAINT "PK_client" PRIMARY KEY (nif)
) INHERITS (usuaris);

CREATE TABLE administradors (
    CONSTRAINT "PK_administrador" PRIMARY KEY (nif)
) INHERITS (usuaris);

CREATE TABLE impressores (
    id_impressora INTEGER,
    nom VARCHAR(25),
    serialNumber VARCHAR(50),
    CONSTRAINT "PK_impressora" PRIMARY KEY (id_impressora)
);

CREATE TABLE operaris (
    id_impressora INTEGER,
    CONSTRAINT "PK_operari" PRIMARY KEY (nif),
    CONSTRAINT "FK_operari_impressora" FOREIGN KEY (id_impressora) REFERENCES impressores(id_impressora)
        ON DELETE RESTRICT ON UPDATE CASCADE
) INHERITS (usuaris);

CREATE SEQUENCE comandes_id_comanda_seq;
CREATE TABLE comandes (
    id_comanda INTEGER NOT NULL DEFAULT nextval('comandes_id_comanda_seq'),
    id_client VARCHAR(9),
    id_enviament INTEGER,
    dataCreacio VARCHAR (20),
    dataEnviament VARCHAR (20),
    estat estat_comanda,
    CONSTRAINT "PK_comanda" PRIMARY KEY (id_comanda),
    CONSTRAINT "FK_comanda_client" FOREIGN KEY (id_client) REFERENCES clients(nif)
        ON DELETE CASCADE ON UPDATE CASCADE
);
ALTER SEQUENCE comandes_id_comanda_seq OWNED BY comandes.id_comanda;

CREATE TABLE detall_comandes (
    id_comanda INTEGER,
    id_objecte3d INTEGER,
    id_impressora INTEGER,
    pathFitxer3d VARCHAR (255),
    costUnitari NUMERIC(2),
    quantitat INTEGER,
    subTotal NUMERIC(2),
    CONSTRAINT "PK_detall_comanda" PRIMARY KEY (id_comanda, id_objecte3d),
    CONSTRAINT "FK_detall_comanda_comanda" FOREIGN KEY (id_comanda) REFERENCES comandes(id_comanda)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE SEQUENCE enviaments_id_enviament_seq;
CREATE TABLE enviaments (
    id_enviament INTEGER NOT NULL DEFAULT nextval('enviaments_id_enviament_seq'),
    id_comanda INTEGER,
    CONSTRAINT "PK_enviament" PRIMARY KEY (id_enviament),
    CONSTRAINT "FK_enviament_comanda" FOREIGN KEY (id_comanda) REFERENCES comandes(id_comanda)
        ON DELETE RESTRICT ON UPDATE CASCADE
);
ALTER SEQUENCE enviaments_id_enviament_seq OWNED BY enviaments.id_enviament; 
*/

/*
INSERT INTO clients
    (nif, nom, cognoms, contrasenya, email, carrer, cp, municipi, provincia, pais, telefon, infoTargetaBanc)
VALUES
    ('12345678A', 'Pere', 'Mates Flaques', '$2a$10$Gg172ksGOCWVakFdstaw6.Uls7hH2hpELhyMJu4V5ZplvNhIeDwc6', 'pmates@abc.eu', 'C. Llarg, 23', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '935689878', '2013895545623781120654'),
    ('87564321Z', 'Joan', 'Roures Tremps', crypt('password', gen_salt('bf', 10)), 'jroures@@xyz.com',  'C. Curt, 12', '08080', 'Salt', 'Girona', 'Catalunya', '666123564', '2983895545623781120654'),
    ('56123789F', 'Marta', 'Gomis Turner', crypt('password', gen_salt('bf', 10)), 'mgomis@hot.xxx', 'C. Bonic, 67', '28004', 'Madrid', 'Madrid', 'Espanya', '915689756', '2017895545623781120654'),
    ('32564781R', 'Maria', 'Llopis Bull', crypt('password', gen_salt('bf', 10)), 'mllopis@gob.es', 'C. Lleig, 45', '50620', 'Teruel', 'Teruel', 'Espanya', '978236587', '2519895545623781120654'),
    ('15975356T', 'Pepa', 'Santxis Diaz', crypt('password', gen_salt('bf', 10)), 'psantxis@gencat.cal', 'Avgda. Diagonal, 666', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '678521265', '2783895545623781120654'),
    ('85236974U', 'Josep', 'Trueta Grau', crypt('password', gen_salt('bf', 10)), 'jtrueta@trus.cu', 'Pl. del Centre, 3', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '632458945', '2013895545623781120654');
*/

-- for @Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
/*
with new_usuari as (
  insert into usuaris (nif, nom, cognoms, contrasenya) values ('87564321Z', 'Joan', 'Roures Tremps', crypt('password', gen_salt('bf', 10)))
  returning nif
)
insert into clients (email, carrer, cp, municipi, provincia, pais, telefon, infoTargetaBanc, nif)
values 
('jroures@@xyz.com', 'C. Curt, 12', '08080', 'Salt', 'Girona', 'Catalunya', '666123564', '2983895545623781120654',
  (select nif from new_usuari)
);
*/
-- for @Inheritance(strategy=InheritanceType.JOINED)
WITH data(nif, nom, cognoms, contrasenya, userrole, email, carrer, cp, municipi, provincia, pais, telefon, infoTargetaBanc) AS (
    VALUES
        ('12345678A', 'Pere', 'Mates Flaques', crypt('password1', gen_salt('bf', 10)), 'ROLE_CLIENT', 'pmates@abc.eu', 'C. Llarg, 23', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '935689878', '2013895545623781120654'),
        ('87564321Z', 'Joan', 'Roures Tremps', crypt('password2', gen_salt('bf', 10)), 'ROLE_CLIENT', 'jroures@@xyz.com',  'C. Curt, 12', '08080', 'Salt', 'Girona', 'Catalunya', '666123564', '2983895545623781120654'),
        ('56123789F', 'Marta', 'Gomis Turner', crypt('password3', gen_salt('bf', 10)), 'ROLE_CLIENT', 'mgomis@hot.xxx', 'C. Bonic, 67', '28004', 'Madrid', 'Madrid', 'Espanya', '915689756', '2017895545623781120654'),
        ('32564781R', 'Maria', 'Llopis Bull', crypt('password4', gen_salt('bf', 10)), 'ROLE_CLIENT', 'mllopis@gob.es', 'C. Lleig, 45', '50620', 'Teruel', 'Teruel', 'Espanya', '978236587', '2519895545623781120654'),
        ('15975356T', 'Pepa', 'Santxis Diaz', crypt('password5', gen_salt('bf', 10)), 'ROLE_CLIENT', 'psantxis@gencat.cal', 'Avgda. Diagonal, 666', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '678521265', '2783895545623781120654'),
        ('85236974U', 'Josep', 'Trueta Grau', crypt('password6', gen_salt('bf', 10)), 'ROLE_CLIENT', 'jtrueta@trus.cu', 'Pl. del Centre, 3', '08080', 'Barcelona', 'Barcelona', 'Catalunya', '632458945', '2013895545623781120654')
   )
, new_usuari AS (
	INSERT INTO usuaris (nif, nom, cognoms, contrasenya, userrole)
	SELECT nif, nom, cognoms, contrasenya, userrole FROM data
	RETURNING nif
   )
INSERT INTO clients (email, carrer, cp, municipi, provincia, pais, telefon, infoTargetaBanc, nif)
SELECT email, carrer, cp, municipi, provincia, pais, telefon, infoTargetaBanc, nif
FROM   data
JOIN new_usuari USING (nif);


/*
INSERT INTO comandes
    (id_client, id_enviament, dataCreacio, dataEnviament, estat)
VALUES
    ('12345678A', NEXTVAL('enviaments_id_enviament_seq'), '2017-10-05', NULL, 'pendent'),
    ('12345678A', NEXTVAL('enviaments_id_enviament_seq'), '2017-10-05', NULL, 'pendent'),
    ('87564321Z', NEXTVAL('enviaments_id_enviament_seq'), '2017-10-06', '2017-10-07', 'enviada');

INSERT INTO detall_comandes
    (id_comanda, id_objecte3d, id_impressora, pathFitxer3d, costUnitari, quantitat, subTotal)
VALUES
    (1, 1, 1, '/var/clients/client1/dsfsdf.stl', 43.40, 1, 43.40),
    (1, 2, 2, '/var/clients/client1/gjjghjghn.stl', 44.50, 1, 44.50),
    (2, 3, 1, '/var/clients/client2/vragdfhdf.stl', 12.70, 2, 25.40),
    (3, 4, 2, '/var/clients/client3/fdhjfngfn.stl', 33.80, 1, 33.80);
   
INSERT INTO impressores
    (id_impressora, nom, serialNumber)
VALUES
    (1,'Makerbot Replicator+', '3434-3444-2333-T'),
    (2,'Makerbot Replicator Z18', '6767-5788-3453-X');

INSERT INTO operaris
    (nif, nom, cognoms, contrasenya, id_impressora)
VALUES
    ('77777777A', 'Primer', 'Operari', '$2a$10$Gg172ksGOCWVakFdstaw6.Uls7hH2hpELhyMJu4V5ZplvNhIeDwc6', 1),
    ('33333333A', 'Segon', 'Operari', '$2a$10$Gg172ksGOCWVakFdstaw6.Uls7hH2hpELhyMJu4V5ZplvNhIeDwc6', NULL);
*/

WITH data(nif, nom, cognoms, contrasenya, userrole, id_impressora) AS (
    VALUES
        ('56781234N', 'Operari1', 'operari operari', crypt('1234', gen_salt('bf', 10)), 'ROLE_OPERARI', 1),
        ('67892345N', 'Operari2', 'operari operari', crypt('1234', gen_salt('bf', 10)), 'ROLE_OPERARI', NULL)
   )
, new_operari AS (
	INSERT INTO usuaris (nif, nom, cognoms, contrasenya, userrole)
	SELECT nif, nom, cognoms, contrasenya, userrole FROM data
	RETURNING nif
   )
INSERT INTO operaris (id_impressora, nif)
SELECT id_impressora, nif
FROM   data
JOIN new_operari USING (nif);

WITH data(nif, nom, cognoms, contrasenya, userrole) AS (
    VALUES
        ('12345678N', 'Admin1', 'admin admin', crypt('1234', gen_salt('bf', 10)), 'ROLE_ADMIN'),
        ('23456789N', 'Admin2', 'admin admin', crypt('1234', gen_salt('bf', 10)), 'ROLE_ADMIN')
   )
, new_administrador AS (
	INSERT INTO usuaris (nif, nom, cognoms, contrasenya, userrole)
	SELECT nif, nom, cognoms, contrasenya, userrole FROM data
	RETURNING nif
   )
INSERT INTO administradors (nif)
SELECT nif
FROM   data
JOIN new_administrador USING (nif);

INSERT INTO roles (role) VALUES ('ROLE_USER');
INSERT INTO roles (role) VALUES ('ROLE_ADMIN');
INSERT INTO roles (role) VALUES ('ROLE_OPERARI');
INSERT INTO roles (role) VALUES ('ROLE_CLIENT');


