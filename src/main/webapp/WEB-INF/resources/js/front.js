if ($.cookie("pintis_csspath")) {
    $('link#pintis-stylesheet').attr("href", $.cookie("pintis_csspath"));
}
if ($.cookie("pintis_layout")) {
    $('body').addClass($.cookie("pintis_layout"));
}

$(function () {

    inici();
    sliders();
    sencer();
    menuSliding();
    utils();
    anima();
    counters();
    demo();

});


function demo() {

    if ($.cookie("pintis_csspath")) {
	$('link#pintis-stylesheet').attr("href", $.cookie("pintis_csspath"));
    }

    $("#colour").change(function () {

	if ($(this).val() !== '') {

	    var pintis_csspath = 'css/style.' + $(this).val() + '.css';

	    $('link#pintis-stylesheet').attr("href", pintis_csspath);

	    $.cookie("pintis_csspath", pintis_csspath, {expires: 365, path: '/'});
	}

	return false;
    });
    
    $("#layout").change(function () {

	if ($(this).val() !== '') {

            var pintis_layout = $(this).val();

            $('body').removeClass('wide');
            $('body').removeClass('boxed');
            
            $('body').addClass(pintis_layout);

	    $.cookie("pintis_layout", pintis_layout, {expires: 365, path: '/'});
	}

	return false;
    });    
}


function inici() {
    if ($('#slider').length) {
	var slider = $("#slider");

	$("#slider").sliderpintiprint({
	    autoPlay: 3000,
	    items: 4,
	    itemsDesktopSmall: [900, 3],
	    itemsTablet: [600, 3],
	    itemsMobile: [500, 2]
	});
    }

}


function sliders() {
    if ($('.slider-pintiprint').length) {

	$(".part1").sliderpintiprint({
	    items: 6,
	    itemsDesktopSmall: [990, 4],
	    itemsTablet: [768, 2],
	    itemsMobile: [480, 1]
	});

	$(".part2").sliderpintiprint({
	    items: 4,
	    itemsDesktopSmall: [990, 3],
	    itemsTablet: [768, 2],
	    itemsMobile: [480, 1]
	});

	$('.part3').sliderpintiprint({
	    navigation: true, 
	    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
	    slideSpeed: 300,
	    paginationSpeed: 400,
	    autoPlay: true,
	    stopOnHover: true,
	    singleItem: true,
	    afterInit: '',
	    lazyLoad: true
	});

	$('.homepage').sliderpintiprint({
	    navigation: false, 
	    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
	    slideSpeed: 2000,
	    paginationSpeed: 1000,
	    autoPlay: true,
	    stopOnHover: true,
	    singleItem: true,
	    lazyLoad: false,
	    addClassActive: true,
	    afterInit: function () {
		
	    },
	    afterMove: function () {
		//animador();
	    }
	});
    }

}





function menuSliding() {


    $('.dropdown').on('show.bs.dropdown', function (e) {

	if ($(window).width() > 750) {
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();

	}
	else {
	    $(this).find('.dropdown-menu').first().stop(true, true).show();
	}
    }

    );
    $('.dropdown').on('ocultar.bs.dropdown', function (e) {
	if ($(window).width() > 750) {
	    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
	}
	else {
	    $(this).find('.dropdown-menu').first().stop(true, true).ocultar();
	}
    });

}



function anima() {
    delayTime = 0;
    $('[data-animate]').css({opacity: '0'});
    $('[data-animate]').waypoint(function (direction) {
	delayTime += 150;
	$(this).delay(delayTime).queue(function (next) {
	    $(this).toggleClass('animated');
	    $(this).toggleClass($(this).data('animate'));
	    delayTime = 0;
	    next();
	   
	});
    },
	    {
		offset: '90%',
		triggerOnce: true
	    });

    $('[data-animate-hover]').hover(function () {
	$(this).css({opacity: 1});
	$(this).addClass('animated');
	$(this).removeClass($(this).data('animate'));
	$(this).addClass($(this).data('animate-hover'));
    }, function () {
	$(this).removeClass('animated');
	$(this).removeClass($(this).data('animate-hover'));
    });

}

function animador() {

    var delayTimeSlider = 400;

    $('.slider-item:not(.active) [data-animate-always]').each(function () {

	$(this).removeClass('animated');
	$(this).removeClass($(this).data('animate-always'));
	$(this).stop(true, true, true).css({opacity: 0});

    });

    $('.slider-item.active [data-animate-always]').each(function () {
	delayTimeSlider += 500;

	$(this).delay(delayTimeSlider).queue(function (next) {
	    $(this).addClass('animated');
	    $(this).addClass($(this).data('animate-always'));

	    console.log($(this).data('animate-always'));

	});
    });



}


function counters() {

    $('.counter').counterUp({
	delay: 10,
	time: 1000
    });

}



function pictureZoom() {

    $(' .image, .image').each(function () {
	var imgHeight = $(this).find('img').height();
	$(this).height(imgHeight);
    });
}


function sencer() {

    var screenWidth = $(window).width() + "px";

    if ($(window).height() > 500) {
	var screenHeight = $(window).height() + "px";
    }
    else {
	var screenHeight = "500px";
    }


    $("#intro, #intro .item").css({
	width: screenWidth,
	height: screenHeight
    });
}
function utils() {

    

    $('[data-toggle="tooltip"]').tooltip();

    
    
    $('.box.clickable').on('click', function (e) {

	window.location = $(this).find('a').attr('href');
    });
    

    $('.external').on('click', function (e) {

	e.preventDefault();
	window.open($(this).attr("href"));
    });
   

    $('.scroll-to, .scroll-to-top').click(function (event) {

	var full_url = this.href;
	var parts = full_url.split("#");
	if (parts.length > 1) {

	    scrollTo(full_url);
	    event.preventDefault();
	}
    });
    function scrollTo(full_url) {
	var parts = full_url.split("#");
	var trgt = parts[1];
	var target_offset = $("#" + trgt).offset();
	var target_top = target_offset.top - 100;
	if (target_top < 0) {
	    target_top = 0;
	}

	$('html, body').animate({
	    scrollTop: target_top
	}, 1000);
    }
}


$.fn.normalitzador = function () {
    $('.same-height-row').each(function () {

	var maxHeight = 0;
	var children = $(this).find('.same-height');
	children.height('auto');
	if ($(window).width() > 768) {
	    children.each(function () {
		if ($(this).innerHeight() > maxHeight) {
		    maxHeight = $(this).innerHeight();
		}
	    });
	    children.innerHeight(maxHeight);
	}

	maxHeight = 0;
	children = $(this).find('.same-height-always');
	children.height('auto');
	children.each(function () {
	    if ($(this).height() > maxHeight) {
		maxHeight = $(this).innerHeight();
	    }
	});
	children.innerHeight(maxHeight);

    });
}

$(window).load(function () {

    windowWidth = $(window).width();

    $(this).normalitzador();
    pictureZoom();
});
$(window).resize(function () {

    newWindowWidth = $(window).width();

    if (windowWidth !== newWindowWidth) {
	setTimeout(function () {
	    $(this).normalitzador();
	    sencer();
	    pictureZoom();
	}, 205);
	windowWidth = newWindowWidth;
    }

});
