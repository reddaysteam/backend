<%-- 
    Document   : Layout
    Created on : 29/10/2017, 11:53:34
    Author     : Llorenç Garcia Martinez
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ca">
    <head>
        <link type="image/x-icon" href='<spring:url value="/resources/img/favicon.ico"/>' rel="icon">
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <c:if test="${not empty metaDescription}">
            <meta name="description" content="${metaDescription}">
        </c:if>
        

        <!-- CDNS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

        <!-- Latest compiled and minified CSS -->
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
        <!-- Optional theme -->
<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->

        <link href='<spring:url value="/resources/styles/style.pintiprint.css"/>' rel="stylesheet" id="pintis-stylesheet">
        <link href='<spring:url value="/resources/styles/main.css"/>' rel="stylesheet">
        
        <!-- slider pintiprint css -->
        <link href='<spring:url value="/resources/styles/slider.pintiprint.css"/>' rel="stylesheet">
        <link href='<spring:url value="/resources/styles/slider.pintis.css"/>' rel="stylesheet">
        
        <link rel="shortcut icona" href='<spring:url value="/resources/img/favicona.ico"/>' type="image/x-icona" />
        <link rel="apple-touch-icona" href='<spring:url value="/resources/img/apple-touch-icona.png"/>' />
        <link rel="apple-touch-icona" sizes="57x57" href='<spring:url value="/resources/img/apple-touch-icona-57x57.png"/>' />
        <link rel="apple-touch-icona" sizes="72x72" href='<spring:url value="/resources/img/apple-touch-icona-72x72.png"/>' />
        <link rel="apple-touch-icona" sizes="76x76" href='<spring:url value="/resources/img/apple-touch-icona-76x76.png"/>' />
        <link rel="apple-touch-icona" sizes="114x114" href='<spring:url value="/resources/img/apple-touch-icona-114x114.png"/>' />
        <link rel="apple-touch-icona" sizes="120x120" href='<spring:url value="/resources/img/apple-touch-icona-120x120.png"/>' />
        <link rel="apple-touch-icona" sizes="144x144" href='<spring:url value="/resources/img/apple-touch-icona-144x144.png"/>' />
        <link rel="apple-touch-icona" sizes="152x152" href='<spring:url value="/resources/img/apple-touch-icona-152x152.png"/>' />

        <title>Pint-i-Print<c:if test="${not empty metaTitle}"> - ${metaTitle}</c:if></title>
    </head>
    <body>
        <div id="all">
            <header>
                <div id="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-5 contact">
                                <p class="hidden-sm hidden-xs">Telèfon 93 456 00 03</p>
                                <p class="hidden-md hidden-lg">
                                    <span title="TRUCAR PER TELÈFON"> <a href="tel:934560003" data-animate-hover="pulse"><i class="fa fa-phone" ></i></a> </span> 
                                </p>
                            </div>
                            <div class="login">
                                <a href='<spring:url value="/private/index.html"/>'><i class="fa fa-sign-in "></i> <span class="hidden-xs text-lowercase " class="logina" >Entrar </span></a>
                                <a href='<spring:url value="/signup"/>'><i class="fa fa-user"></i> <span class="hidden-xs mayusculator">  Registrar-se</span></a>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- MENU -->
                <div class="navbar-fixad-top" data-spy="fixador" data-offset-top="200">
                    <div class="navbar navbar-default pintimenu" role="navigation" id="navbar">
                        <div class="container">
                            <div class="navegador">
                                <a class="navbar-brand home" href='<spring:url value="/"/>'>
                                    <img src='<spring:url value="/resources/img/logo3.png"/>' alt="Pintiprint logo" class="hidden-xs hidden-sm">
                                    <img src='<spring:url value="/resources/img/logo3.png"/>' alt="Pintiprint logo" class="visible-xs visible-sm"><span class="sr-only"></span>
                                </a>
                                <div class="navbar-buttons">
                                    <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                        <span class="sr-only">...</span>
                                        <i class="fa fa-align-justify"></i>
                                    </button>
                                </div>
                            </div>
                            <!--/.navegador -->

                            <div class="navbar-collapse collapse" id="navigation">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active">
                                        <a href='<spring:url value="/"/>'>Inici</a>
                                    </li>
                                    <li class="">
                                        <a href='<spring:url value="/#serveis"/>'>Serveis</a>
                                    </li>
                                    <li class="">
                                        <a href="<spring:url value='/faq'/>">FAQ</a>
                                    </li>
                                    <li class="">
                                        <a href="<spring:url value='/contacte'/>">Contacte</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
                      
            <section class="content">
                <jsp:doBody />
            </section>

            <div class="footer-section">
                <div class="footer">
                    <div class="container">
                        <div class="col-md-4 footer-one">
                            <div class="foot-logo">
                                <img src="<spring:url value='/resources/img/logo.png'/>">
                            </div> 

                                <p>Empresa sorgida del treball final del Curs CFGS Desenvolupament d'aplicacions Web.
                                </p>
                            <div class="social-icons"> 
                                <a href="https://www.facebook.com/Pintiprint-171281356953644/"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                                <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                                <a href="https://www.pinterest.es/pintiprint/"><i id="social-tw" class="fa fa-pinterest-square fa-3x social"></i></a>
                                <a href="https://www.youtube.com/channel/UCtfqUc1vXyVFYZ4jYIwNvZA?view_as=subscriber"><i id="social-gp" class="fa fa-youtube-square fa-3x social"></i></a>
                                <a href="mailto:pintiprint@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                            </div>

                        </div>
                        <div class="col-md-2 footer-two">
                            <h5>Serveis</h5>
                            <ul>
                                <li>
                                  <a href="<spring:url value='/signup'/>"> Registre</a> 
                                </li>
                                <li>
                                   <a href="#preu"> Tarifes</a> 
                                </li>
                                <li>
                                  <a href="#transport"> Transport</a> 
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-2 footer-three">
                            <h5>Enllaços</h5>
                            <ul>
                                <li>
                                  <a href="<spring:url value='#funcionament'/>"> Funcionament</a> 
                                </li>
                                <li>
                                   <a href="<spring:url value='/faq'/>"> FAQ</a> </li>
                                <li>
                                   <a href="<spring:url value='/contacte'/>"> Contacte</a> 
                                </li>
                            </ul>
                        </div>

                        <div class="col-md-4 footer-four">
                            <h5>Contacti</h5>
                            <ul>
                                <li>
                                    <i class="fa fa-map-marker"></i>Avinguda Paral·lel núm 34, Barcelona. 
                                </li>
                                <li>
                                    <i class="fa fa-envelope-o"></i>pintiprint@gmail.com </li>
                                <li><i class="fa fa-phone"></i>934 34 5981</li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="footer-bottom container">
                        <div class="row">
                            <div class="col-sm-6 ">
                                <div class="copyright-text">
                                    <p>&copy; 2017 Pint-i-Print, SCCL. </p>
                                </div>
                            </div> 
                            <div class="col-sm-6  ">
                                <div class="copyright-text pull-right">
                                    <p> 
                                       <a href="<spring:url value="/"/>">Home</a> 
                                     | <a href="<spring:url value='/lopd'/>">Avís legal</a>
                                     | <a href="<spring:url value='/signup'/>">Registre</a> 
                                     | <a href="<spring:url value='/accessibilitat'/>">Accessibilitat</a> 
                                    </p>
                                </div>                  
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                        
        <div class="cookies-advice" id="headerCookiesAdvice">
            <div class="container"
                <div class="row">
                    <p class="col-md-10">
                        Pint-i-print utilitza "cookies" per millorar l'experiència de navegació. Si segueixes navegant entendrem que ho acceptes.
                        <a href="<spring:url value='/cookies.html'/>" class="header-lnk">Més Informació</a>
                    </p>
                    <p class="col-md-2 text-right header-button">
                        <a href="" class="header-button-close">D'acord</a>
                    </p>
                </div>
            </div>
        </div>
        <!-- jQuery -->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
        <!-- Latest compiled and minified JavaScript -->
<!--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->

        <!-- .js   -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

        <script src='<spring:url value="/resources/js/jquery.cookie.js"/>'></script>
        <script src='<spring:url value="/resources/js/front.js"/>'></script>
        <script src='<spring:url value="/resources/js/slider.pintiprint.min.js"/>'></script>
        <script src='<spring:url value="/resources/js/preu.js"/>'></script>
        <script src='<spring:url value="/resources/js/count.js"/>'></script>
        <script src='<spring:url value="/resources/js/cookies.js"/>'></script>
    </body>
</html>