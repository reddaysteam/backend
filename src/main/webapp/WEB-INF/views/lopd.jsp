<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <div class="container">
        <h2>Av�s legal i pol�tica de privacitat</h2>
        <h3 class="1st">Informaci� general de la seu electr�nica o lloc web Pint-i-Print.cat </h3>

        <p><strong>Pint-i-Print.cat</strong> <em>(<a href="http://www.pintiprint.cat/">http://www.pintiprint.cat</a>) </em> �s un domini d'Pint-i-Print SCCL (Pint-i-Print) amb NIF B-xxxxxxxx, inscrita al registre mercantil de la prov�ncia de Barcelona (volum xxxxx, foli xxx, full B-xxxxxx) i domicili a Barcelona, adre�a <a href="mailto:info@pintiprint.cat">info@printiprint.cat</a>. Mitjan�ant el domini Pint-i-Print.cat, l'empresa gestiona el seu portal web, sense perjudici que puntualment o de manera permanent hi pugui haver altres adreces electr�niques que tamb� permetin accedir als continguts i serveis prestats mitjan�ant el lloc web Pint-i-Print.cat.</p>

        <h3>Acc�s dels usuaris</h3>
        <p>L'acc�s i/o �s dels portals de Pint-i-Print atribueix la condici� d'usuari i implica l'acceptaci� de les condicions d'acc�s i �s que consten en aquest av�s legal, aix� com la pol�tica de privacitat i dades personals. Les citades condicions seran d'aplicaci� independentment de les condicions generals de contractaci� i de comer� electr�nic que resultin d'aplicaci� en les funcionalitats que ofereix el portal. L'usuari es compromet a utilitzar els serveis i la informaci� facilitada al portal Pint-i-Print.cat sota criteris de bona fe. </p>
        <p>L'acc�s inicial al portal �s gratu�t. Tanmateix, hi poden haver funcionalitats o serveis subjectes al pagament d'un preu en les condicions que en cada cas s'estableixen.</p>
        <p>En cas que per utilitzar determinades funcions o serveis sigui necessari que l'usuari es registri, aquest registre s'efectuar� mitjan�ant formularis de dades i amb subjecci� a les prevencions de la legislaci� de protecci� de dades personals que es descriuen en aquest av�s legal.<br>
            <br>
            Per utilitzar el portal els menors d'edat hauran de disposar del perm�s dels pares, mares o representants legals, que en seran responsables.</p>
        <h3>Protecci� dels drets de propietat intel�lectual i industrial</h3>
        <p>S�n titularitat de Pint-i-Print els drets de propietat intel�lectual i industrial del portal Pint-i-Print.cat, que inclou els continguts i elements gr�fics, el seu codi font, disseny, estructura de navegaci�, bases de dades i altres elements que formen part del lloc web. Per tant, correspon en exclusiva a Pint-i-Print l'exercici, en exclusiva, dels drets d'explotaci� en qualsevol forma i, en concret, dels drets de reproducci�, distribuci�, comunicaci� p�blica i transformaci�. Qualsevol �s o reproducci� que els usuaris facin d'aquests continguts, o d'altres que s'hi puguin incloure en el futur, s'han de fer seguint les previsions i respectant les limitacions de la normativa de propietat intel�lectual i en qualsevol cas amb l'autoritzaci� de Pint-i-Print, sense perjudici dels l�mits que estableix el reial decret 1/1996, del 12 d'abril, pel qual s'aprova el text ref�s de la llei de propietat intel�lectual.</p>
        <p><strong>Reserva de drets:</strong> Pint-i-Print explicita una reserva de drets amb car�cter general respecte al conjunt dels continguts difosos en el lloc web que s�n objecte de propietat intel�lectual i industrial. D'acord amb el previst a l'article 32 del test ref�s de la llei de propietat intel�lectual, Pint-i-Print s'oposa expressament a la utilitzaci� de qualsevol dels continguts de les publicacions de Pint-i-Print en qualsevol format, amb la finalitat de fer ressenyes o revistes de premsa amb finalitat comercial, sense la pr�via autoritzaci� expressa de Pint-i-Print.</p>
        <p>Aquest av�s legal no suposa cap cessi� de drets a favor de l'usuari en relaci� a cap dels elements integrants de la web o dels seus continguts.</p>
        <p>No s'autoritza en cap cas:</p>
        <ul type="disc">
            <li>La presentaci� d'una p�gina del portal Pint-i-Print.cat o qualsevol contingut d'aquesta p�gina en una finestra que no pertany a Pint-i-Print.cat per mitj� de qualsevol t�cnica o procediment incloent-hi <em>framing, inline linking, </em>etc.</li>
            <li>Una extracci� d'elements del lloc web que causi perjudici a Pint-i-Print.cat conforme a les disposicions vigents. </li>
            <li>L'�s comercial dels continguts del lloc web Pint-i-Print.cat. </li>
            <li>L'�s de marques o signes distintius, logotips, combinacions de colors, estructura, disseny i, en general, s�mbols distintius de qualsevol naturalesa, propietat de Pint-i-Print, sense el coneixement i l'autoritzaci� corresponent de l'empresa. </li>
            <li>L'�s de continguts en activitats il�l�cites, il�legals o contr�ries a la bona fe i l'ordre p�blic. </li>
            <li>Difondre continguts o propaganda de car�cter racista o xen�fob, pornogr�fic, il�legal o que atempti contra els drets humans. </li>
            <li>Provocar danys en els sistemes f�sics i l�gics de Pint-i-Print, dels seus prove�dors o de terceres persones; introduir o difondre a la xarxa virus inform�tics o altres sistemes f�sics o l�gics que siguin susceptibles de provocar danys anteriorment esmentats. </li>
            <li>Intentar accedir i, en el seu cas, utilitzar els comptes de correu electr�nic d'altres usuaris i modificar o manipular els seus missatges. </li>
        </ul>
        <p>L'usuari podr� visualitzar els continguts d'aquest portal, copiar-los, imprimir-los en el seu ordinador o qualsevol altre suport nom�s per al seu �s personal i privat.</p>
        <h3>Protecci� de dades de car�cter personal </h3>
        <p><em>A) Dades recollides a trav�s de formularis</em> </p>
        <p>En el cas que faciliteu les vostres dades personals a trav�s del portal Pint-i-Print.cat, el seu tractament se sotmet a les previsions de la llei org�nica 15/1999, de 13 de desembre, de protecci� de dades de car�cter personal, i la resta de normativa aplicable. Les dades es tractaran per a les finalitats indicades en els avisos corresponents i s'incorporaran en un fitxer de dades personals. Per exercir els drets d'acc�s, rectificaci�, cancel�laci� o oposici� amb relaci� a les vostres dades de car�cter personal tractades per Pint-i-Print, en els termes i les condicions previstes en el t�tol III de la LOPD, us podeu adre�ar a <a href="mailto:lopd@pintiprint.cat">lopd@pintiprint.cat</a>, o a l'adre�a de Pint-i-Print que consta al principi d'aquest av�s legal.</p>

        <p>
            Finalitat del fitxer: la informaci� facilitada s?utilitzar� per a les finalitats indicades en el registre p�blic de l'Ag�ncia Espanyola de Protecci� de Dades Personals, i que s�n:

        </p><ul>
            <li>- Gesti� de clients, comptable, fiscal i administrativa</li>
            <li>- Publicitat i prospecci� comercial</li>
            <li>- Comer� electr�nic</li>
        </ul>
        <p></p>

        <p><em>B) Dades de navegaci�</em></p>
        <p>El sistema de navegaci� i el programari necessaris per al funcionament d'aquest lloc web recullen, de manera est�ndard, algunes dades la transmissi� de les quals �s impl�cita en l'�s dels protocols de comunicaci� d'internet. En aquesta categoria de dades hi ha l'adre�a IP o nom de domini de l'ordinador utilitzat per la persona usu�ria per connectar-se al lloc web, l'adre�a URL del recurs demanat, l'hora, el m�tode utilitzat per a la consulta al servidor, la mida de l'arxiu obtingut en la resposta, el codi num�ric que indica l'estat de la resposta al servidor i altres par�metres relatius al sistema operatiu de l'entorn inform�tic de la persona usu�ria.</p>
        <p>Aquesta informaci� no s'associa a persones usu�ries concretes i s'utilitza amb la finalitat exclusiva d'obtenir informaci� estad�stica sobre l'�s del lloc web Pint-i-Print.cat.</p>
        <p>Pint-i-Print.cat no utilitza galetes (<em>cookies</em>) o altres mitjans de naturalesa an�loga per al tractament de dades personals que permetin la identificaci� de persones f�siques concretes, usu�ries de la seu electr�nica. L'�s d'aquests mitjans es reserva exclusivament per recollir informaci� t�cnica per facilitar a les persones usu�ries l'accessibilitat i l'exploraci� segura i eficient de la seu electr�nica d'Pint-i-Print.cat o per a mesures i dades d'audi�ncies an�nimes.</p>
        <h3>Responsabilitat en relaci� amb els continguts </h3>
        <p>Pint-i-Print es reserva el dret de modificar els continguts de la web i d'eliminar-los, aix� com tamb� de limitar o impedir l'acc�s  a la web, ja sigui temporalment o definitivament, sense notificaci� pr�via.<br>
            Pint-i-Print no es fa responsable de l'acc�s t�cnic, la informaci� o els continguts d'altres p�gines web a les quals remet o amb les quals es pugui enlla�ar des del portal Pint-i-Print.cat.<br>
            Pint-i-Print no s'identifica necess�riament amb les opinions expressades pels seus col�laboradors.</p>
        <h3>Dret d'exclusi� </h3>
        <p>Pint-i-Print SCCL es reserva el dret de denegar o retirar l'acc�s als seus llocs web i/o els serveis oferts sense necessitat d'av�s previ, a inst�ncia pr�pia o d'un tercer, a aquells usuaris que incompleixin les presents condicions generals.</p>
        <h3>Generalitats </h3>
        <p>Pint-i-Print SCCL perseguir� l'incompliment de les presents condicions aix� com qualsevol utilitzaci� indeguda dels seus llocs web i exercir� totes les accions civils i penals que li puguin correspondre per dret.</p>
        <p>Pint-i-Print no pot garantir que el portal i el servidor estiguin lliures de virus, per la qual cosa no es fa responsable dels danys o perjudicis que es puguin causar per aquestes anomalies o qualsevol altra d'�ndole t�cnica.</p>
        <h3>Modificaci� de les presents condicions i durada </h3>
        <p>Pint-i-Print podr� modificar en qualsevol moment les condicions aqu� determinades i seran degudament publicades com apareixen aqu�. La vig�ncia de les citades condicions anir� en funci� de la seva exposici� i estaran vigents fins que siguin modificades per altres degudament publicades.</p>
        <h3>Legislaci� aplicable i jurisdicci� </h3>
        <p>La relaci� entre Pint-i-Print i els usuaris es regir� per la normativa espanyola vigent i qualsevol controv�rsia se sotmetr� als jutjats i tribunals de la ciutat de Barcelona.</p>
    </div>
</t:Layout>