<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <div class="container">
        <h4>Pol�tica de cookies</h4>

        <p>D'acord amb la normativa vigent, Pint-i-print facilita als usuaris informaci�
            relativa a les 'cookies' que utilitza i els motius del seu �s, aix� com
            sol�licita el seu consentiment per poder utilitzar-les.</p>

        <p>La web de Pint-i-print utilitza "cookies" pr�pies i de tercers per tal
            d'oferir una millor experi�ncia de navegaci� i un millor servei.
            No obstant l'usuari t� l'opci� d'impedir la generaci� de 'cookies' i 
            l'eliminaci� de les mateixes mitjan�ant la selecci� de la corresponent 
            opci� al seu navegador. En cas de bloquejar l'�s de les "cookies" al seu
            navegador, �s possible que alguns serveis o funcionalitats de la p�gina 
            web no estiguin disponibles.</p>


        <h4>Qu� �s una 'cookie'?</h4>

        <p>Les 'cookies' constitueixen una eina empleada pels servidors web per
            emmagatzemar i recuperar informaci� relativa als usuaris. S�n petits
            arxius de text que les p�gines web envien al navegador i que 
            s'emmagatzemen al terminal de l'usuari, ja sigui un ordinador, un 
            tel�fon m�bil o una tauleta. Aquesta informaci� permet recuperar 
            informaci� com per exemple el seu usuari registrat, si en disposa, 
            dades de connexi� a les xarxes socials o dades de prefer�ncies de 
            navegaci�. Aquestes dades s�n emmagatzemades per a ser recordades 
            quan l'usuari visita de nou la p�gina.</p>

        <h4>Com s'utilitzen les 'cookies' i quin tipus d'informaci� guarden?</h4>

        <p>Al navegar per la p�gina web de Pint-i-print, l'usuari est� acceptant 
            que es puguin instal.lar 'cookies' al seu terminal que permetin 
            con�ixer la seg�ent informaci�:</p>

        <ul>
            <li>Informaci� estad�stica de l'�s de la web</li>
            <li>El 'login' de l'usuari per mantenir la sessi� activa</li>
            <li>El format de web que prefereix per accedir als dispositius m�bils</li>
            <li>Informaci� sobre els anuncis que es mostren als usuaris</li>
            <li>L'activitat que ha dut a terme l'usuari al web (p�gines vistes,
                cerques, etc.)</li>
            <li>Dades de connexi� a les xarxes socials per als usuaris que 
                accedeixen amb el seu usuari de Facebook o Twitter</li>
        </ul>

        <p>En cas de bloquejar l'�s de 'cookies' del seu navegador alguns dels 
            serveis de la p�gina web deixaran d'estar disponibles.</p>
    </div>
</t:Layout>