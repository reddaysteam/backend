<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <section>
        <div class="jumbotron">
            <div class="container">
                <h2>Comandes</h2>
            </div>
        </div>
    </section>
    
    <section class="container">
        
        <div th:if="${message}">
            <h2 th:text="${message}"/>
        </div>

        <div>
            <form method="POST" enctype="multipart/form-data" action="/">
                <table>
                    <tr><td>File to upload:</td><td><input type="file" name="file" /></td></tr>
                    <tr><td></td><td><input type="submit" value="Upload" /></td></tr>
                </table>
            </form>
        </div>

        <div>
            <ul>
                <c:forEach items="${files}" var="file">
                    <li>
                        <a href=" <spring:url value= '${file}' /> ">${file}<a/>
                    </li>
                </c:forEach>
            </ul>
        </div>
                
    </section>
</t:Layout>
