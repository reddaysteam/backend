<%-- 
    Document   : signup
    Created on : 29/10/2017, 11:34:40
    Author     : Lloren� Garcia Martinez
--%>

<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <section class="container">
        <form:form modelAttribute="formClient" class="col-md-9 box" action="signup" method="post">
            <fieldset>
                <div class="row">
                    <legend class="col-md-12 mayusculator">
                        <h2>Formulari de registre</h2>
                        <hr>
                    </legend>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <spring:hasBindErrors name="formClient">
                        <div>
                            <div class="col-md-12">
                                <div class="form-group alert alert-danger" role="alert">
                                    <form:errors path="*" element="div"/>
                                </div>
                            </div>
                        </div>
                    </spring:hasBindErrors>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="uppercase" for="nom">Nom</label>
                            <form:input id="nom" placeholder="Nom propi" path="nom" type="text" class="form-control" pattern="^\D+$" maxlength="25" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="uppercase" for="cognoms">Cognoms</label>
                            <form:input id="cognoms" placeholder="Cognoms" path="cognoms" type="text" class="form-control" maxlength="50" pattern="^\D+$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="nif">NIF</label>
                            <form:input id="nif" path="nif" placeholder="12345678A" type="text" class="form-control" maxlength="9" pattern="(^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$)|(^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$)" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="telefon">Tel�fon</label>
                            <form:input id="telefon" path="telefon" placeholder="666555777" type="text" class="form-control" pattern="^(\+\d{1,3}\d{7,16})|(\d{7,20})$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="uppercase" for="contrasenya">Contrassenya</label>
                            <form:input id="contrasenya" path="contrasenya" placeholder="* * * * * *" type="password" class="form-control" maxlength="60" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="uppercase" for="carrer">Carrer</label>
                            <form:input id="carrer" path="carrer" type="text" placeholder="Carrer, n�mero i porta" class="form-control" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="municipi">Municipi</label>
                            <form:input id="municipi" path="municipi" placeholder="Nom municipi" type="text" class="form-control" pattern="^\D+$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="cp">Codi Postal</label>
                            <form:input id="cp" path="cp" placeholder="01234" type="text" class="form-control" pattern="^\d+$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="provincia">Provincia</label>
                            <form:input id="provincia" placeholder="Provincia" path="provincia" type="text" class="form-control" pattern="^\D+$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label class="uppercase" for="pais">Pais</label>
                            <form:input id="pais" path="pais" placeholder="Pais" type="text" class="form-control" pattern="^\D+$" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="uppercase" for="email">Adre�a electr�nica</label>
                            <form:input id="email" path="email" placeholder="adre�a@email.com" type="email" class="form-control" required="required"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <hr>
                            <p class="lead">Pol�tica de privacitat</p>
                            <p>En compliment del que disposa la Llei Org�nica 15/1999 de 13 de desembre de Protecci� de Dades
                                de Car�cter Personal, i en la Llei 34/2002 de 11 de juliol de Serveis de la Societat de la Informaci�
                                i de Comer� Electr�nic i altres normes aplicables, la societat Mercantil, Pint-i-Print, informa als
                                Usuaris de la p�gina web, sobre la seva Pol�tica de privacitat i de protecci� de dades
                                de car�cter personal, recollides dels Usuaris d'aquest Lloc Web, que estiguin interessats a formalitzar un registre.
                            </p>
                            <p>
                                <label class="lopd">
                                    <input id="lopd" name="lopd" value="true" type="checkbox" required="required"> He llegit i accepto la <a href="<spring:url value='/lopd'/>" target="_blank">politica de privacitat</a> 
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="box-footer form-group">
                            <input type="reset" id="btnAdd" class="btn btn-default"
                                   value ="< Esborra"/>
                            <input type="submit" id="btnAdd" class="btn btn-template-main next pull-right"
                                   value ="Continua >"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </section>
                           
    <!-- orig design -->
    <!--
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="box">
                    <div class="col-md-9 clearfix" id="checkout">
                        <div class="box">
                            <h2 class="mayusculator">Fomulari de registre</h2>
                            <p class="lead">Pol�tica de privacitat</p>
                            <p>En compliment del que disposa la Llei Org�nica 15/1999 de 13 de desembre de Protecci� de Dades de Car�cter Personal, i en la Llei 34/2002 de 11 de juliol de Serveis de la Societat de la Informaci� i de Comer� Electr�nic i altres normes aplicables, la societat mercantil, Pint-i-Print, informa als Usuaris de la p�gina web, sobre la seva Pol�tica de privacitat i de protecci� de dades de car�cter personal, recollides dels Usuaris d'aquest Lloc Web, que estiguin interessats a formalitzar un registre.</p>
                            <p class="text-muted">Acceptar <a href="acceptat.html">pol�tica de privacitat</a></p>
                            <form action="">
                                <input type="checkbox" name="privacitat" value="Accepto_privacitat"> He llegit i accepto la pol�tica de privacitat <br>
                            </form>
                            <hr>
                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="nom">NOM</label>
                                            <input type="text" placeholder="Nom propi" class="form-control" id="nom">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="cognoms">COGNOMS</label>
                                            <input type="text" placeholder="Cognoms" class="form-control" id="cognoms">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="NIF">IDENTIFICADOR FISCAL</label>
                                            <input type="text" placeholder="NIF O CIF" class="form-control" id="nif">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="TEL">TEL�FON</label>
                                            <input type="text" placeholder="N�mero de tel�fon" class="form-control" id="telf">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password">CONTRASENYA</label>
                                            <input type="text" placeholder="* * * * * *" class="form-control" id="pass">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="carrer">CARRER</label>
                                            <input type="text" placeholder="Carrer, n�mero i porta" class="form-control" id="carrer">
                                        </div>

                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="provincia">PROVINCIA</label>
                                            <select class="form-control" id="provincias" name="provincies" onchange="modificar()">
                                                <option value="0">- Seleccioni PROVINCIA -</option>  id="state"></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label placeholder="Nom municipi" for="municipi">MUNICIPI</label>
                                            <select class="form-control" id="municipis" name="municipis">
                                                <option value="0">- Seleccioni MUNICIPI -</option>  id="country"></select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="cp">CODI POSTAL</label>
                                            <input type="text" placeholder="Codi postal" class="form-control" id="cp">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="pais">PAIS</label>
                                            <input type="text" placeholder="Pais" class="form-control" id="pais">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">ADRE�A ELECTR�NICA</label>
                                            <input placeholder="adre�a @ email.com " type="text" class="form-control" id="email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="esquerra">
                                    <a href="enrera.html" class="btn btn-default"><i class="fa fa-pintibron-left"></i>ESBORRA</a>
                                </div>
                                <div class="dreta">
                                    <button type="submit" class="btn btn-template-main">CONTINUA<i class="fa fa-pintibron-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
</t:Layout>