<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <section>
        <iframe width="100%" height="400px" frameborder="0" style="border:0"
        src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJNw5ou1CipBIRbeTilHa2kbk&key=AIzaSyBUsjiYqvbBLN-TtbglZeJlCoszIeY8WiY" allowfullscreen>
        </iframe>
    </section>

    <div id="content">
        <div class="container" id="contact">
            <section>
                <div class="row">
                    <div class="col-md-8">
                        <div class="heading">
                            <h2>Pint-i-Print</h2>
                        </div>

                        <p class="lead">
                            El nostre taller est�obert tots els dies de dilluns a divendres, 
                            per� no dubteu a posar-vos en contacte amb nosaltres en qualsevol moment. 
                            <strong><p>Teniu alguna pregunta o us interessa imprimir algun projecte? </strong>
                        </p>
                        <p>Poseu-vos en contacte amb nosaltres a trav�s del formulari de contacte i us tornem a contactar tan aviat com puguem.<br><br></p>


                        <div class="heading">
                                <h3>Contacte per WhatsAPP</h3>
                        </div>

                            
                        <div class="row">
                            <div class="col-sm-11">
                                <p>Gr�cies a l'aplicaci� de WhatsAPP, podreu resoldre la majoria de dubtes d'una manera r�pida i directa. Nom�s ha d'afegir el tel�fon 654234091 als contactes.</p>

                                <p>Una manera interactiva de poder enviar arxius de impressi� i comprovar de forma r�pida si s�n imprimibles o requereixen realitzar alguna modifici�.</p>
                                <a href="https://web.whatsapp.com">
                                    <img src="<spring:url value="/resources/img/whatsapp-messenger.png"/>" alt="Imatge Whatsapp que porta la web" class="img-responsive">
                                </a>
                            </div>
                                    
                                    
                        </div>
                    </div>
                        <div class="col-md-4"></div>

                            <div class="col-md-4">
                                <div class="heading">
                                    <h3>Adre�a</h3>
                                    <p>
                                    Avinguda Paral�lel n�mero 34
                                    <br>08002
                                    <br>Barcelona
                                    <br>
                                    <strong>Catalunya</strong>
                                    </p>
                                </div>
                                <div class="heading">
                                    <h3>Taller</h3>
                                    <p class="text-muted">
                                    El nostre taller est� obert al p�blic de dilluns a divendres.
                                    De tant en tant, es fan cursos i confer�ncies sobre impressi� 3D
                                    </p>
                                    <p><strong>Tel�fon: 93 498 65 23</strong>
                                    </p>
                                </div>

                                <div class="heading">
                                    <h3>Contacte per email</h3>
                                    <p class="text-muted">
                                    Si s'estima m�s contactar per correu directament o vol afegir
                                    algun arxiu per comprobar si �s imprimible, facin's un email a:
                                    </p>
                                     
                                    <strong><a href="mailto:">taller@pintiprint.cat</a></strong>
                            
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

</t:Layout>