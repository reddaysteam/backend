<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
<div class="container">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1>FAQ</h1>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 clearfix">
                    <section>
                        <p class="lead">Aqu� tenim algunes respostes a preguntes que potser pot tenir vost� en aquest moment:</p>

                        <div class="panel-group" id="accordion">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq1">1. Quant trigar� a arribar la meva comanda?</a>
                                    </h4>
                                </div>
                                <div id="faq1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            La majoria dels treballs s'enviaran dins dels 4 dies laborables despr�s d'aprovar la seva idonetat. Si us plau, comproveu la p�gina d'ordre de producte, all� s'especifica el temps aproximat de lliurament de cada comanda. L'enviament per part de Correos triga d'1 a 5 dies laborals per arribar-vos segons la seva ubicaci�.</p>
                                        <ul>
                                            <li>Pen�nsula: 1-3 dies.</li>
                                            <li>Balears: 2-4 dies.</li>
                                            <li>Canaries: 1-5 dies.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq2">2. Com puc veure les meves comandes?</a>
                                    </h4>
                                </div>
                                <div id="faq2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Dins de l'�rea privada tens una secci� anomenada "Les meves comandes" on trobar�s totes les comandes que hagis realitzat,
                                        independentment del seu estat. I si cliques una podr�s accedir al detall de la comanda.
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq3">3. Puc enviar documents amb formats diferents?</a>
                                    </h4>
                                </div>
                                <div id="faq3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Absolutament!<br> Si heu creat documents amb diferents programes, no us amoineu, podeu enviar els vostres projectes, perque tenim compatibilitat amb la majoria dels formats d'arxiu actuals.
                                        <p>Si teniu qualsevol dubte, podeu consultar i enviar l'arxiu previament. Podem processar versions de InDesign, CorelDRAW, blender, Illustrator, Photoshop, Freehand i, qualsevol arxiu de sortida dels principals programes de disseny 3D.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq4">4. Quina privacitat i seguretat de dades oferim?</a>
                                    </h4>
                                </div>
                                <div id="faq4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Pint-i-Print no recopila les seves dades per processar i compartir amb cap altra empresa o lloc web. Les dades personals s'utilitzen per contactar-vos quan sigui necessari. La nostra web utilitza tecnologia actualitzada de servidor segur. La vostra comanda s'envia i es gestiona de forma segura al nostre servidor i assegura la integritat de les dades. Els vostres arxius i dissenys mai seran compartits o exposats a tercers de forma directa o indirecta.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#faq5">5. Per qu� m'he de registrar?</a>
                                    </h4>
                                </div>
                                <div id="faq5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Per accedir als serveis d'impressi� �s necessari el registre, aquest, us permet realitzar la comanda de projectes i enmagatzemar un hist�ric de totes les vostres comandes.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-muted">Si no heu trobat la resposta als vostres dubtes, no dubteu en posar-vos en contacte amb
                            <br>Pint-i-Print: <a href="<spring:url value="/contacte"/>">CONTACTE</a>
                        </p>
                    </section>
                </div>

                <!-- Menu lateral -->
                <div class="col-sm-3">
                    <div class="panel panel-default sidebar-menu">
                        <div class="panel-heading">
                            <h3 class="panel-title">ACCESSOS DIRECTES</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked">
                                <li>
                                    <a href="private/index.html">Area Privada</a>
                                </li>
                                <li>
                                    <a href="<spring:url value="/contacte"/>">Contactar</a>
                                </li>
                                <li>
                                    <a href="<spring:url value="/lopd"/>">Privacitat</a>
                                </li>
                                <li>
                                    <a href="<spring:url value="/signup"/>">Registre</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</t:Layout>