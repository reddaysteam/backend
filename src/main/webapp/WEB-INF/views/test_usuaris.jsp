<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <section>
        <div class="jumbotron">
            <div class="container">
                <h1>Usuaris</h1>
                <p>Els usuaris de la BD</p>
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <c:forEach items="${usuaris}" var="usuari">
                <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
                    <div class="thumbnail">
                        <div class="caption">
                            <p>${usuari.nif}</p>
                            <p>${usuari.nom}</p>
                            <p>${usuari.cognoms}</p>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </section>


    <footer class="container-fluid text-center">
        <p>Pint-i-Print, SCCL - </p>
    </footer>
</t:Layout>
