<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    
    <div id="content" class="accessibilitat">
        <div class="container">
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h1>ACCESSIBILITAT</h1>
                        </div>
                            <div class="col-md-3">
                                <img class="img-responsive img-circle" src="<spring:url value='/resources/img/logotip_accessibilitat.png'/>" alt="Logotip d'accessibilitat" >
                            </div>
                            <p>

                                L'Objectiu de Pint-i-Print �s facilitar i fer arribar l'acc�s a tots els Continguts i serveis que Pint-i-Print ofereix, primant la senzillesa i l'accessibilitat de la informaci�. Per aix�, em concebut la majoria de continguts tenint en compte els est�ndards d'usabilitat i accessibilitat. 
                            </p>

                            <p>
                                Intentant que totes les persones puguin navegar sense trobar barreres d'acc�s; independentment del sexe, la edat o les capacitats f�siques. Tampoc a d'haver difer�ncies en quant a diferents velocitats connexi� o amb el tipus de dispositiu amb el que es conecta.
                            </p>

                            <p>
                                Seguint les directrius  de Web Accessible WAI (AA), Pint-i-Print ha desenvolupat el seu web sobre les pautes d'accessibilitat m�s b�siques proporcionades per la WAI Web Accesibility Initiative (Organisme del W3C que informa, investiga i fomenta l'accessibilitat web). 
                            </p>


                            <p>
                                S'han revisat i verificat punts d'accessibilitat com: navegaci� sense estils, incorporaci� de textos alternatius a les imatges, etiquetes amb nom de contingut, navegaci� per tabulaci� i sense ratol� i �s de narradors.
                            </p>

                        </div>

                        <div class="col-md-12">
                            <div class="heading">
                                <h2>Tecnologies de les vistes</h2>
                            </div>
                            <ul class="ul-iconas">
                                <li>
                                    <i class="fa fa-check"></i>P�gines desenvolupades amb HTML 5 amb marcat sem�ntic i estructurat segons els standards del W3C.
                                </li>

                                <li>
                                    <i class="fa fa-check"></i>Framework bootstrap 3.2, que �s acceptat per la majoria de navegadors i unifica estils a nivell multiplataforma.
                                </li>

                                <li>
                                    <i class="fa fa-check"></i>Estils amb CSS3 per a la capa de presentaci�, elaborats sota la recomanaci� del W3C.
                                </li>
                            </ul>
                        </div>

                    </div>


            </section>

            <section>
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h3>Format de les p�gines</h3>
                        </div>

                        <div>
                            <ul class="ul-iconas">
                                <li>
                                    <i class="fa fa-check"></i>Els continguts s'adapten a les diferents mides de pantalla, adequant els textos i les imatges de forma adequada per a la seva correcta lectura.
                                </li>

                                <li>
                                    <i class="fa fa-check"></i>La navegaci� es pot realitzar �nicament amb tabulaci�, mitjan�ant les tecles de tabulaci� dels teclats estandard i sense haver de fer servir cap ratol��.
                                </li>

                                <li>
                                    <i class="fa fa-check"></i>Creaci� de m�ltiples enlla�os per a facilitar la navegaci� i acc�s als continguts i serveis de Pint-i-Print.
                                </li>

                                <li>
                                    <i class="fa fa-check"></i>P�gines estructurades amb HTML 5 i bootstrap, realitzant la separaci� del disseny i de les vistes amb fulls d'estil en cascada CSS.
                                </li>
                                    
                                <li>
                                    <i class="fa fa-check"></i>Descripci� en totes les imatges mitjan�ant l'atribut ALT d'HTML.
                                </li>
                            </ul>

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="heading">
                            <h3>Multiplataforma</h3>
                        </div>
                        <ul class="ul-iconas">
                            <li>
                                <i class="fa fa-check"></i>Els continguts de la web de Pint-i-Print es poden visualitzar de forma correcta amb diferents navegadors web; hem fet proves amb els m�s utilitzats: Chrome, Firefox i Safari.
                            </li>

                            <li>
                                <i class="fa fa-check"></i>La web �s accessible amb la majoria de dispositius amb connexi� a Internet: ordinadors, m�bils i tablets, en tots ells, s'asoleix una correcta visualitzaci� de continguts i una navegaci� fluida.
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-12">
                        <div class="heading">
                            <h3>Enlla�os externs</h3>
                        </div>

                        <p>
                            Per ampliar la informaci� i extendre els serveis de Pint-i-Print a altres entorns com xarxes socials, s'han creat espais personalitzats a les xarxes socials m�s conegudes, de les quals, trobar� enlla�os al footer de totes p�gines externes de la part p�blica.
                            Al cliclar, l'usuari entrar� a la xarxa social o el servei de missatgeria configurat al seu dispositiu.
                        </p>
                    </div>
                </div>
            </section>
        </div>
    </div>
</t:Layout>