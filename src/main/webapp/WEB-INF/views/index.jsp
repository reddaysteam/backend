<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<t:Layout>
    <!-- SLIDER -->
    <section>
        <div class="home-pintiprint">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="homepage slider-pintiprint">
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-5 right">
                                <p>
                                    <img src='<spring:url value="/resources/img/logo3.png"/>' alt="Logotip de Pint-i-print">
                                </p>
                                <h2>Pintar, printar i lliurar</h2>
                                <p>Pots utilitzar diferents softwares de disseny en 3D, 
                                    ja que aquests funcionaran amb qualsevol impressora 3D de Pint-i-Print.</p>
                            </div>
                            <div class="col-sm-7">
                                <img class="img-responsive" src='<spring:url value="/resources/img/lupa.png"/>' alt="Imatge ilustrativa de la secci� Pintar, printar i lliurar">
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-7 text-center">
                                <img class="img-responsive" src='<spring:url value="/resources/img/temple.png"/>' alt="Imatge ilustrativa de la secci� D�na forma a les idees">
                            </div>
                            <div class="col-sm-5">
                                <h2>Dona forma a les idees </h2>
                                <ul class="list-style-none">
                                    <p>
                                        <br>Hem provat tots els formats 3D existents, de manera que es garanteix la correcta impressi�.
                                    </p>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-sm-5 right">
                                <h1>Dissenya</h1>
                                <ul class="list-style-none">
                                    <li>Llibertat per crear la teva idea</li>
                                    <li>Compatible amb tots els formats d'arxius</li>
                                    <li>Experts que poden assesorar i millorar</li>
                                    <li>Diferents tipus de materials</li>
                                </ul>
                            </div>
                            <div class="col-sm-7">
                                <img class="img-responsive" src='<spring:url value="/resources/img/eines.png"/>' alt="Imatge ilustrativa de la secci� Eines">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Secci� per editar (tinc moltes parts fetes) -->
    <section id="serveis" class="bar background-white">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-desktop"></i></div>
                            <h3>Servei Online</h3>
                            <p>Realitzaci� de projectes de forma virtual, vost� nom�s ha de pujar l'arxiu que cont� el disseny 3D i nosaltres ens encarregarem d'imprimir-lo i enviar-lo.</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-print"></i></div>
                            <h3>Impressi� 3D</h3>
                            <p>Disposem d'una gran varietat d'impressores que poden realitzar impressions en 3 dimensions en diferents formats i grandaries.</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-globe"></i></div>
                            <h3>Compatibilitat</h3>
                            <p>Els nostres sistemes d'impressi� permeten la impressi� mitjan�ant diferents tipus d'arxius procedents dels diferents programaris de disseny.</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-lightbulb-o"></i></div>
                            <h3>Consultoria</h3>
                            <p>Sempre t� a la seva dispossi� tot l'equip de Pint-i-Print per poder consultar qualsevol dubte i realitzar millores en els seus projectes.</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-folder-open"></i></div>
                            <h3>Area privada</h3>
                            <p>Disposem d'una area privada on podr� coleccionar i administrar tots els seus projectes. Nom�s t� que registrar-se mitjant�ant un formulari.</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box-simple">
                            <div class="icona"><i class="fa fa-users"></i></div>
                            <h3>EQUIP</h3>
                            <p>L'equip de Pint-i-Print est� format per professionals amb una gran pasi� pel m�n de la impressi� 3D i ofereixen les �ltimes novetats del mercat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div id="preu" class="preu">
            <div class="preus animated swing">
                <div class="thumbnail animated pulse infinite">
                    15� per arxiu
                </div>
                <div class="title">
                    Faci clic per obtenir m�s detalls
                </div>
                <div class="conte">
                    <div class="subtitul">
                        Detalls del preu fix de 15 euros per projecte.
                    </div>
                    <ul>
                        <li>
                            <div class="fa fa-check"></div>
                            Qualsevol color d'impressi�.
                        </li>
                        <li>
                            <div class="fa fa-check"></div>
                            Diferents tamanys i n�mero de peces.
                        </li>
                        <li>
                            <div class="fa fa-check"></div>
                            Qualsevol tipus d'arxiu 3D.
                        </li>
                        <li>
                            <div class="fa fa-close"></div>
                            No inclou retoc del disseny.
                        </li>
                        <li>
                            <div class="fa fa-close"></div>
                            No aplicable a peces de gran format.
                        </li>
                        <li>
                            <div class="fa fa-close"></div>
                            No inclou el preu de 8 euros del transport.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div id="funcionament" class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading text-center">
                        <h2>Funcionament de Pint-i-Print</h2>
                        <p><br>
                            El proc�s de realitzaci� d'un projecte a Pint-i-Print es pot resumir en els seg�ents pasos:
                        </p>
                    </div>

                    <ul class="pasos">
                        <li>
                            <div class="pasos-image">
                                <img class="img-circle img-responsive" src="<spring:url value='/resources/img/Pas1_registre_impressio3D_online.png'/>" alt="Pas 1: imatge que simbolitza el registre">
                            </div>
                            <div class="pasos-panel heading">
                                <div class="pasos-heading ">
                                    <h2 class="subheading">Registre</h2>
                                </div>
                                <div class="pasos-body">
                                    <p class="text-muted">
                                        <br>Per poder operar i realitar els seus projectes nom�s s'ha de registrar a trav�s d'un simple formulari que trobar� a la secci� de registre.
                                    </p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </li>

                        <li class="pasos-inverted">
                            <div class="pasos-image">
                                <img class="img-circle img-responsive" src="<spring:url value='/resources/img/Pas2_gestio_impressio_3D.png'/>" alt="Pas 2: imatge que simbolitza la pujada del disseny">
                            </div>
                            <div class="pasos-panel">
                                <div class="pasos-heading heading">
                                    <h2 class="subheading">Disseny</h2>
                                </div>
                                <div class="pasos-body">
                                    <p class="text-muted">
                                        Un cop registrat ja pot dissenyar i enviar el seu projecte 3D sense importar el format utilitzat, ja que printiprint est� preparat per gestionar la majoria de formats exportats pels programaris de disseny 3D.
                                    </p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </li>
                        <li>
                            <div class="pasos-image">
                                <img class="img-circle img-responsive" src="<spring:url value='/resources/img/Pas3_impressio_online_de_peces_3D.png'/>" alt="Pas 3: Imatge d'una impressi� 3D">
                            </div>
                            <div class="pasos-panel">
                                <div class="pasos-heading heading">
                                    <h2 class="subheading">Impressi� 3d</h2>
                                </div>
                                <div class="pasos-body">
                                    <p class="text-muted">
                                        El pas m�s important, un cop pujat l'arxiu amb el disseny 3D, els nostres operaris verificaran la idoneetat del disseny i la vialitat del projecte. Fet aix� es procedir� a realitzar la impressi�.
                                    </p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </li>
                        <li class="pasos-inverted">
                            <div class="pasos-image">
                                <img class="img-circle img-responsive" src="<spring:url value='/resources/img/Pas4_enviament_impressio3D.png'/>" alt="Pas 4: imatge d'un cami� de transport">
                            </div>
                            <div class="pasos-panel">
                                <div class="pasos-heading heading">
                                    <h2 class="subheading">Lliurament</h2>
                                </div>
                                <div class="pasos-body">
                                    <p class="text-muted">
                                        Un cop realitzada la impressi� i la verificaci� del control de qualitat de Pint-i-Print, procedirem a l'enviament de cada pe�a. Sempre garantint el correcte manipulat i empaquetat, perque arribi en perfectes condicions. 
                                    </p>
                                </div>
                            </div>
                            <div class="line"></div>
                        </li>
                        <li>
                            <div class="pasos-image">
                                <img class="img-circle img-responsive" src="<spring:url value='/resources/img/Ajuda_impressio_3D.png'/>" alt="Pas 5: imatge que representa el servei postventa">
                            </div>
                            <div class="pasos-panel">
                                <div class="pasos-heading heading">
                                    <h2 class="subheading">Informaci�</h2>
                                </div>
                                <div class="pasos-body">
                                    <p class="text-muted">
                                        Si t� qualsevol dubte, sobre el treball de Pint-i-Print o vol ampliar informaci� en un �rea concreta, sempre pot conctactar amb l'equip de Pint-i-Print. Tamb� el podrem assessorar en la viabilitat de projectes o per si sorgeix alguna incid�ncia en la gesti� o el transport.
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="bar pentacular">
        <div class="container">
            <div class="row showcase">
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="iconi text-uppercase"><i class="fa fa-print"></i>
                        </div>
                        <h4><br><div class="counter" data-count="6"></div><br>

                            Impressores</h4><br>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="iconi"><i class="fa fa-users"></i>
                        </div>
                        <h4><br><div class="counter" data-count="22"></div><br>

                            Clients</h4><br>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item">
                        <div class="iconi"><i class="fa fa-cogs"></i>
                        </div>
                        <h4><br><div class="counter" data-count="42"></div><br>

                            Projectes</h4><br>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="item text-center">
                        <div class="iconi"><i class="fa fa-graduation-cap"></i>
                        </div>
                        <h4><br><div class="counter" data-count="22"></div><br>

                            Cursos i tallers</h4><br>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Pinti MON -->
    <section class="bar monpintiprint no-mb color-white text-center">
        <div class="dark-mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="icona icona-lg"><i class="fa fa-globe"></i>
                    </div>
                    <h1 class="text-uppercase">Vols descobrir el m�n de Pin-i-Print?</h1>
                    <h2 class="text-uppercase ">L'�ltima tecnolog�a en impressi� 3D gestionada desde casa. <h2>

                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="heading">
                        <h2>Missi�</h2>
                    </div>
                    <ul class="ul-iconas">
                        <li><i class="fa fa-check"></i>Fer arribar la impressi� 3D a tothom.</li>
                        <li><i class="fa fa-check"></i>Crear un servei online i marcar la difer�ncia.</li>
                        <li><i class="fa fa-check"></i>Materialitzar objectes creats i desitjats.</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="heading">
                        <h2>Els nostres valors</h2>
                    </div>

                    <ul class="ul-iconas">
                        <li><i class="fa fa-check"></i>Open Source: potenciar el talent col.lectiu.</li>
                        <li><i class="fa fa-check"></i>Passi�: estem compromessos amb la innovaci� del 3D.</li>
                        <li><i class="fa fa-check"></i>Qualitat: sempre busquem i apostem per l'excel�ncia.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="transport" class="#transport bar no-mb color-white padding-big text-center-sm">
        <div class="container transport">
            <div class="row transport">
                <div class="col-md-3 text-center">
                    <img src="<spring:url value='/resources/img/Mapa_enviament_peces_3D.png'/>" alt="" class="img-responsive">
                </div>
                <div class="col-md-4">
                    <h2 class="text-uppercase">Transport peninsular i insular</h2>
                    <p class="lead mb-small">Nom�s 8 euros per lliurament:

                        <br>Pen�nsula: 1-3 dies.
                        <br>Balears: 2-4 dies.
                        <br>Canaries: 1-5 dies.
                    </p>
                </div>
                <div class="col-md-5 text-center">
                    <img src="<spring:url value='/resources/img/enviament_de_impressions_3D.png'/>" alt="Imatge medis de transport terra, mar i aire. " class="img-responsive">
                    <p class="mayusculator"> Preu fix de 8 euros per comanda</p>
                </div>
            </div>
        </div>
    </section>
</t:Layout>
